/**
 * SPOJ NSTEPS solution
 * Go (2010-07-14)
 * Author: Jimmy Mabey
 */

package main

import (
	"fmt"
	"os"
)

func pointNumber(x, y int) (int, os.Error) {
	if x == y {
		// Double y (or x) and subtract 1 if y (or x) is odd
		return (y * 2) - (y % 2), nil
	} else if x-2 == y {
		// Double y, add 2, and subtract 1 if y is odd
		return (y * 2) + 2 - (y % 2), nil
	}

	return 0, os.NewError(fmt.Sprintf("main: Invalid point (%d, %d)", x, y))
}

func main() {
	var casesLeft, x, y int

	fmt.Scanf("%d", &casesLeft)

	for ; casesLeft > 0; casesLeft -= 1 {
		fmt.Scanf("%d %d", &x, &y)

		if number, err := pointNumber(x, y); err != nil {
			fmt.Println("No Number")
		} else {
			fmt.Println(number)
		}
	}
}
