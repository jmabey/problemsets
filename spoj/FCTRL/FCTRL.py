"""
SPOJ INTSS solution
Python 3.2.3
Author: Jimmy Mabey
"""

import sys

def Z(number):
	zeroes = 0
	while number >= 5:
		number = number // 5
		zeroes += number
	return zeroes

if __name__ == '__main__':
	for _ in range(int(sys.stdin.readline())):
		print(Z(int(sys.stdin.readline())))
