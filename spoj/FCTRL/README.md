A "trailing zero" is caused by a number having a factor of 10.  By calculating
`n!` for increasing values of `n`, it becomes apparent that one or more trailing
zeros are added when `n` reaches a multiple of 5.  In fact, when `n = 5^x` for
some integer `x > 0`, `x` trailing zeroes are added.

This has to do with `n!` having `Z(n)` factors that are 10.  The prime factors
of 10 are 2 and 5, thus it makes sense that when `n` reaches some multiple of 5,
`n!` has additional prime factors that are 5.  Since `n!` has many factors that
are even, it has plenty of prime factors that are 2, which "pair" with every 5
prime factor to create a 10 factor.

Dividing `n` by 5 once is the number of factors of `n!` that are multiples of 5.
Dividing `n` by 5 twice is the number of factors of `n!` that are multiples of
`5^2`.  ...and so on.  By repeatedly dividing `n` by 5, we are counting the
number of prime factors of `n!` that are 5 (and thus, factors of `n!` that are
10, and thus the number of trailing zeroes).  
