/**
 * SPOJ ARITH solution
 * Go (2010-07-14)
 * Author: Jimmy Mabey
 */

package main

import (
	"big"
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
)

type Calculator struct {
	operands *[2]big.Int
	operator byte
	lines    []string
}

/**
 * Instantiate a new Calculator.
 */
func NewCalculator(operands *[2]big.Int, operator byte) *Calculator {
	// Enough room for operands, ruler, and solution
	size := 4

	// Multiplication has an additional ruler and one line for each digit in the
	// second operand to show partial results, but only if the second operand
	// has more than one digit
	if length := len(operands[1].String()); operator == '*' && length > 1 {
		size += 1 + length
	}

	return &Calculator{operands, operator, make([]string, size)}
}

/**
 * Return Calculator output as a string.
 */
func (calc *Calculator) Output() string {
	calc.setOperandLines()
	calc.setSolutionLine()

	if calc.operator == '*' && len(calc.operands[1].String()) > 1 {
		calc.setPartialLines()
		calc.setRuler(len(calc.lines) - 2) // Just above solution
	}

	calc.setRuler(2) // Just above solution or partial product results
	calc.alignLines()

	return strings.Join(calc.lines, "\n")
}

/**
 * Store the operands and operator in the Calculator output lines.
 */
func (calc *Calculator) setOperandLines() {
	var buffer bytes.Buffer

	calc.lines[0] = calc.operands[0].String()

	buffer.WriteByte(calc.operator)
	buffer.WriteString(calc.operands[1].String())
	calc.lines[1] = buffer.String()
}

/**
 * Store the solution in the Calculator output lines.
 */
func (calc *Calculator) setSolutionLine() {
	var solution big.Int

	if calc.operator == '+' {
		solution.Add(&calc.operands[0], &calc.operands[1])
	} else if calc.operator == '-' {
		solution.Sub(&calc.operands[0], &calc.operands[1])
	} else {
		solution.Mul(&calc.operands[0], &calc.operands[1])
	}

	calc.lines[len(calc.lines)-1] = solution.String()
}

/**
 * Store partial product result lines in the Calculator output lines.
 */
func (calc *Calculator) setPartialLines() {
	var (
		opDigits = calc.operands[1].String()
		opLength = len(opDigits)
	)

	// From largest to smallest place values (i.e., last to first partial result
	// lines)
	for index, digit := range opDigits {
		var (
			placeValue big.Int
			buffer     bytes.Buffer

			// Amount of right indentation this line has and also the position
			// relative to the first partial result line (right after the first
			// ruler)
			offset = opLength - index - 1
		)

		placeValue.SetString(fmt.Sprintf("%c", digit), 10)
		placeValue.Mul(&placeValue, &calc.operands[0])

		// We're right-padding with spaces, but these will be stripped out later
		buffer.WriteString(placeValue.String())
		buffer.WriteString(strings.Repeat(" ", offset))

		calc.lines[3+offset] = buffer.String()
	}
}

/**
 * Store a ruler in the specified output line, sized according to the lengths of
 * the lines above and below.
 */
func (calc *Calculator) setRuler(index int) {
	siblingLines := []string{calc.lines[index-1], calc.lines[index+1]}

	calc.lines[index] = strings.Repeat("-", findMaxLength(&siblingLines))
}

/**
 * Right-align and trim spaces from all output lines.
 */
func (calc *Calculator) alignLines() {
	maxLength := findMaxLength(&calc.lines)

	for index, line := range calc.lines {
		var buffer bytes.Buffer

		buffer.WriteString(strings.Repeat(" ", maxLength-len(line)))
		buffer.WriteString(strings.TrimSpace(line))

		calc.lines[index] = buffer.String()
	}
}

// -----------------------------------------------------------------------------

/**
 * Reads a test case from input and parses it into the two operands and the
 * operator.
 */
func readCase(reader *bufio.Reader) (operands [2]big.Int, operator byte) {
	var buffer bytes.Buffer

	for {
		if char, err := reader.ReadByte(); err != nil {
			panic(err)
		} else if char != '+' && char != '-' && char != '*' {
			buffer.WriteByte(char)
		} else {
			// Found operator
			operator = char
			break
		}
	}

	operands[0].SetString(buffer.String(), 10)

	// Remainder of line is the second operand
	if line, err := reader.ReadString('\n'); err != nil {
		panic(err)
	} else {
		operands[1].SetString(line, 10)
	}

	return
}

/**
 * Return the length of the longest line in the given slice.
 */
func findMaxLength(strings *[]string) int {
	maxLength := 0

	for _, str := range *strings {
		length := len(str)

		if length > maxLength {
			maxLength = length
		}
	}

	return maxLength
}

// -----------------------------------------------------------------------------

func main() {
	var (
		casesLeft int
		reader    = bufio.NewReader(os.Stdin)
	)

	if _, err := fmt.Scanln(&casesLeft); err != nil {
		panic(err)
	}

	for ; casesLeft > 0; casesLeft-- {
		operands, operator := readCase(reader)
		calculator := NewCalculator(&operands, operator)

		fmt.Println(calculator.Output())
		fmt.Println() // Blank line between cases
	}
}
