"""
SPOJ FCTRL2 solution
Python 3.2.3
Author: Jimmy Mabey
"""

import math
import sys

if __name__ == '__main__':
	for _ in range(int(sys.stdin.readline())):
		print(math.factorial(int(sys.stdin.readline())))
