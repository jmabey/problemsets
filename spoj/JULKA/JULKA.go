// SPOJ JULKA solution
// Go (2010-07-14)
// Author: Jimmy Mabey

package main

import (
	"big"
	"bufio"
	"fmt"
	"os"
)

// numCases is the number of expected input test cases.
const numCases = 10

// =============================================================================

// A Calculator determines the number of apples owned by Klaudia and Natalia,
// given the total number of apples and how many more apples Klaudia has than
// Natalia.
type Calculator struct {
	total, diff      *big.Int
	klaudia, natalia big.Int
	odd              bool
}

// NewCalculator constructs a new Calculator by reading test case data from
// input.
func NewCalculator(reader *bufio.Reader) *Calculator {
	var total, diff big.Int

	if line, err := reader.ReadString('\n'); err != nil {
		panic(err)
	} else {
		total.SetString(line, 10)
	}

	if line, err := reader.ReadString('\n'); err != nil {
		panic(err)
	} else {
		diff.SetString(line, 10)
	}

	return &Calculator{total: &total, diff: &diff}
}

// Calculate determines the number of apples that Klaudia and Natalia have.
func (calc *Calculator) Calculate() {
	var remainder big.Int

	// Solving 2x + diff = total for x, where Klaudia has x + diff apples and
	// Natalia has x apples.  Since we're working with integers for efficient
	// but we may halve odd numbers, we need to account for a "0.5" being
	// introduced.
	calc.natalia.Sub(calc.total, calc.diff).QuoRem(&calc.natalia, big.NewInt(2), &remainder)
	calc.klaudia.Add(&calc.natalia, calc.diff)
	calc.odd = remainder.Int64() == 1
}

// Output prints out the number of apples.
func (calc *Calculator) Output() {
	calc.printNumber(&calc.klaudia)
	calc.printNumber(&calc.natalia)
}

// printNumber outputs the given big.Int and also appends a ".5" if there is an
// apple that was divided in half.
func (calc *Calculator) printNumber(number *big.Int) {
	fmt.Print(number.String())

	if calc.odd {
		fmt.Print(".5")
	}

	fmt.Println()
}

// =============================================================================

func main() {
	reader := bufio.NewReader(os.Stdin)

	for casesLeft := numCases; casesLeft > 0; casesLeft -= 1 {
		calc := NewCalculator(reader)
		calc.Calculate()
		calc.Output()
	}
}
