"""
SPOJ PRIME1 solution
Python 3.2.3
Author: Jimmy Mabey
"""

import math
import sys

def sieve(start, end):
	# Sieve of Eratosthenes, except modified to be segmented
	# http://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
	composites = set([1])

	# Reduce the number of calculations by only sieving odd numbers
	for number in range(3, int(math.sqrt(end)) + 1, 2):
		if number not in composites:
			# Start at closest multiple of number near starting number or
			# number^2, whichever is largest.  number^2 is according to the
			# original Sieve algorithm.  By starting at the closest multiple, we
			# are skipping sieving all primes less than the starting number.
			# This is much faster, because we aren't looking for those primes.
			multiple = max(start - start % number, number * number)

			while multiple <= end:
				composites.add(multiple)
				multiple += number

	for number in range(start, end + 1):
		# Even numbers are a special case and we didn't sieve them for
		# efficiency -- 2 is prime and all other even numbers are composite
		if number == 2 or (number % 2 and number not in composites):
			print(number)


if __name__ == '__main__':
	for _ in range(int(sys.stdin.readline())):
		start, end = (int(number) for number in sys.stdin.readline().split())
		sieve(start, end)
		print()
