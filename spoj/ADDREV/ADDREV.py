"""
SPOJ ADDREV solution
Author: Jimmy Mabey
"""
import sys

class ReversedNumber():
	def __init__(self, reversed):
		self.reversed   = int(reversed)
		self.unreversed = int(str(reversed)[::-1])

	def __str__(self):
		return str(self.reversed)

	def __add__(self, other):
		if not isinstance(other, ReversedNumber):
			return NotImplemented

		sum = self.unreversed + other.unreversed
		return ReversedNumber(str(sum)[::-1])

def main():
	for i in range(int(sys.stdin.readline())):
		nums = []

		for num_str in sys.stdin.readline().split():
			nums.append(ReversedNumber(num_str))

		print(nums[0] + nums[1])

main()
