"""
SPOJ ONP solution
Python 3.2.3
Author: Jimmy Mabey
"""

import sys

def convert(expr):
	# All input uses brackets.  So, we push every variable onto output (since we
	# are essentially only moving the operators around and removing brackets)
	# and the operator onto a seperate stack.  When we encounter a ")", it means
	# an operation has completed and we move the most recent operator to output
	# (since the most recent operation has completed).
	output = []
	operators = []

	for char in expr:
		if char.isalpha():
			output.append(char)
		elif char in ['+', '-', '*', '/', '^']:
			operators.append(char)
		elif char == ')':
			output.append(operators.pop())

	return ''.join(output)


if __name__ == '__main__':
	for _ in range(int(sys.stdin.readline())):
		print(convert(sys.stdin.readline()))
