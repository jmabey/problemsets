"""
SPOJ ADV04L solution
Python 3.2.3
Author: Jimmy Mabey
"""

import sys

class Converter(object):
	def __init__(self):
		# Fibonacci sequence stored in descending order
		self.fibonacci = []

	def calculate(self, limit):
		self.fibonacci = [1, 1]

		# Compare the second largest to the limit, so we have the Fibonacci
		# number just larger than the limit (since we are going to need the next
		# number in the sequence).
		while self.fibonacci[1] < limit:
			self.fibonacci.insert(0, self.fibonacci[0] + self.fibonacci[1])

	def convert(self, miles):
		kilometers = 0

		for index, number in enumerate(self.fibonacci):
			if number <= miles:
				miles -= number
				kilometers += self.fibonacci[index - 1]

		return kilometers


if __name__ == '__main__':
	converter = Converter()
	converter.calculate(1000000000000000)

	for _ in range(int(sys.stdin.readline())):
		miles = int(sys.stdin.readline())
		print(converter.convert(miles))
