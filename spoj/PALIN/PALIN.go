/**
 * SPOJ PALIN solution
 * Go (2010-07-14)
 * Author: Jimmy Mabey
 * 
 * My approach to this is to generate a palindrome by "mirroring" the left half
 * of a number onto the right:
 *
 *     12345678  -->  12344321
 *
 * This generates a palindrome that is close to the given number, but may not
 * necessarily be greater.  If it is greater, than it is guaranteed to be the
 * closet palindrome greater than the given number.  This is due to the
 * mirroring of the digits.
 * 
 * The next largest palindrome is found by incrementing the middle digits.  If
 * we were to increment the palindrome by one, we would also need to increment
 * the highest place-valued digit, which is a much larger number.
 *
 *     12344321  -->  12355321
 *
 * Numbers having an odd length only have one digit, so they need to be handled
 * slightly different.  Care also needs to be taken when we introduce a new
 * digit when incrementing the palindrome, which changes the length and the
 * palindrome's evenness or oddness.
 *
 *     999   -->  1001
 *     9999  -->  10001
 */

package main

import (
	"big"
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
)

type PalinFactory struct {
	number string
}

/**
 * Set the number to be used as a starting point to generate a palindrome.
 */
func (factory *PalinFactory) SetNumber(number string) {
	factory.number = number
}

/**
 * Find the next palindrome after the current number.
 */
func (factory *PalinFactory) Next() string {
	var (
		buffer bytes.Buffer

		numberLen = len(factory.number)
		oddLength = numberLen%2 > 0

		// Greedily split the number into two halves.  Greedily meaning for
		// odd-length numbers, the middle digit belongs to both sides (e.g.,
		// "987" splits into "98" and "87").
		leftSide     = factory.number[0 : (numberLen+1)/2]
		rightSide    = factory.number[numberLen/2:]
		mirroredSide = Reverse(leftSide)

		leftLength = len(leftSide)
	)

	// Before we create a palindrome, we can determine if the resulting
	// palindrome will not be greater than the original number by mirroring the
	// left side and comparing it to the right side. 
	//
	// Using 123456 as an example, the palindrome will be 123321, which is less
	// than 123456.  If we mirror the left side, we get 321.  The right side is
	// 456.  Since 321 <= 456, we know the palindrome will not be greater than
	// the original number.
	if !Greater(mirroredSide, rightSide) {
		// Since it is not greater, we can increment the palindrome by
		// incrementing the left side.  This is essentially incrementing the
		// middle digits of the palindrome, which results in the very next
		// palindrome.  Since we are also incrementing the left side, the
		// resulting palindrome will also be greater than the original number.
		leftSide = Increment(leftSide)

		// One obvious optimization is to set leftSide here and then set it
		// again in the next inner if {}, rather than calling leftValue.String()
		// twice, but that seems to consistently be slower for some reason.

		// If we introduced a new digit, this changes the evenness/oddness of
		// the number, and we need to account for this when constructing the
		// palindrome.  Since we greedily split the number, going from odd to
		// even means we need to drop a digit on the left.
		//
		// For example, the left side of 999 is "99".  The left side of 1001 is
		// "10".  Notice both have two digits.  However, if we increment "99"
		// (as a left side), we get "100", so we need to divide by 10 to get the
		// two digits that will be mirrored to form the palindrome 1001.  This
		// doesn't need to be done when going from odd to even, because the left
		// side has one more digit (left side of 99 is "9", left side of 101 is
		// "10").
		if len(leftSide) != leftLength {
			oddLength = !oddLength

			if !oddLength {
				var leftValue big.Int

				leftValue.SetString(leftSide, 10)
				leftValue.Div(&leftValue, big.NewInt(10))
				leftSide = leftValue.String()
			}
		}

		mirroredSide = Reverse(leftSide)
	}

	buffer.WriteString(leftSide)

	// Mirror the left side onto the right to form a palindrome
	if oddLength {
		// When odd, the left and right sides "overlap" on the middle digit, so
		// don't include in on the right
		buffer.WriteString(mirroredSide[1:])
	} else {
		buffer.WriteString(mirroredSide)
	}

	return buffer.String()
}

// -----------------------------------------------------------------------------

/**
 * Return the given string, but reversed.
 */
func Reverse(input string) string {
	length := len(input)
	output := make([]int, length)

	for index, char := range input {
		output[length-index-1] = char
	}

	return string(output)
}

/**
 * Optimized implementation to check if x > y, where x and y are strings of
 * equal length representing numbers.
 *
 * This is faster than using Cmp() to compare big.Ints and normal string
 * comparison.
 */
func Greater(x string, y string) bool {
	length := len(x)
	for index := 0; index < length; index++ {
		if x[index] > y[index] {
			return true
		}
	}

	return false
}

/**
 * Increment the string representing a number by 1.
 */
func Increment(number string) string {
	var (
		buffer bytes.Buffer
		last   = len(number) - 1
		result = make([]byte, last+1)
	)

	result[last] = number[last] + 1

	for index := last - 1; index >= 0; index-- {
		result[index] = number[index]

		// Carry
		if result[index+1] > '9' {
			result[index]++
			result[index+1] -= 10
		}
	}

	// Final carry that causes result to be one digit longer
	if result[0] > '9' {
		buffer.WriteByte('1')
		result[0] -= 10
	}

	buffer.Write(result)

	return buffer.String()
}

// -----------------------------------------------------------------------------

/**
 * Read a test case from input and return the input number as a string.
 */
func readCase(reader *bufio.Reader) string {
	line, err := reader.ReadString('\n')

	if err != nil {
		panic(err)
	}

	return strings.TrimRight(line, "\n")
}

func main() {
	var (
		casesLeft int
		factory   PalinFactory

		// fmt is too slow for this problem
		reader = bufio.NewReader(os.Stdin)
		writer = bufio.NewWriter(os.Stdout)
	)

	if _, err := fmt.Scanln(&casesLeft); err != nil {
		panic(err)
	}

	for ; casesLeft > 0; casesLeft-- {
		number := readCase(reader)

		factory.SetNumber(number)
		writer.WriteString(factory.Next())
		writer.WriteByte('\n')
	}

	writer.Flush()
}
