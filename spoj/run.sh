#!/bin/bash
# Run solution for a given problem.

DEFAULT_EXT=py
RED="\e[1;31m"
GREEN="\e[1;32m"
GRAY="\e[1;30m"
RESET="\e[0m"

if [[ $# < 1 ]]
then
	echo "Usage: $0 path_to_problem_folder [solution_file_extension]"
	echo "Default extension: $DEFAULT_EXT"
	exit 1
fi

path=$1
extension=${2:-$DEFAULT_EXT}

if [[ ! -d $path ]]
then
	echo "Problem folder $path not found"
	exit 1
fi

problem=`basename $1`
input_file=$path/input.txt
output_file=$path/output.txt

echo "Running solution $problem.$extension for problem $problem"

if [[ $extension == "py" ]]
then
	actual=$(python3 "$path/$problem.py" < $input_file)
elif [[ $extension == "cpp" ]]
then
	g++ "$path/$problem.cpp"

	if [[ ! -f a.out ]]
	then
		echo Failed compilation
		exit 1
	fi

	actual=$(./a.out < $input_file)
	rm a.out
elif [[ $extension == "go" ]]
then
	6g "$path/$problem.go"
	6l "$problem.6"

	if [[ ! -f 6.out ]]
	then
		echo Failed compilation
		exit 1
	fi

	actual=$(./6.out < $input_file)
	rm "$problem.6" 6.out
elif [[ $extension == "awk" ]]
then
	actual=$(awk -f "$path/$problem.awk" < $input_file)
elif [[ $extension == "pl" ]]
then
	actual=$(perl "$path/$problem.pl" < $input_file)
else
	echo "Unrecognized extension $extension"
	exit 1
fi

diff=$(echo "$actual" | diff -u $output_file -)

echo -e "\n$GRAY- ACTUAL ------$RESET"
echo "$actual"
echo -e "$GRAY- EXPECTED ----$RESET"
cat "$output_file"
echo -e "$GRAY- DIFF --------$RESET"
echo "$diff"
echo -e "$GRAY---------------$RESET\n"

if [[ -n $diff ]]
then
	echo -e ${RED}FAIL${RESET}
else
	echo -e ${GREEN}PASS${RESET}
fi
