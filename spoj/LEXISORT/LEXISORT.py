"""
SPOJ LEXISORT solution
Python 3.1.2
Author: Jimmy Mabey
"""

import sys

def main():
	for _ in range(int(sys.stdin.readline())):
		lines = []
		for _ in range(int(sys.stdin.readline())):
			lines.append(sys.stdin.readline().strip('\n'))
		lines.sort()
		print('\n'.join(lines))

if __name__ == '__main__':
	main()
