"""
SPOJ LGIC solution
Python 3.2.3
Author: Jimmy Mabey

The pattern is: aN = N! + 2^N - N

N   aN       N!      + 2^N  - N
--  -------  -------------------
1   2        1       + 2    - 1
2   4        2       + 4    - 2
3   11       6       + 8    - 3
4   36       24      + 16   - 4
5   147      120     + 32   - 5
6   778      720     + 64   - 6
10  3629814  3628800 + 1024 - 10

The trick to this is that the upper bound on N is 10^18, so there's no way to
calculate (10^18)! within this problem's time limit (1 second), let alone its
memory limit.
"""

import math
import sys

if __name__ == '__main__':
	n = int(sys.stdin.readline())
	print(math.factorial(n) + 2**n - n)
