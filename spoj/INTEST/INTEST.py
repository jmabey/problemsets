"""
SPOJ INTEST solution
Python 3.1.2
Author: Jimmy Mabey
"""

import sys

def main():
	meta = sys.stdin.readline().split()
	divisor = int(meta[1])
	count = 0

	for _ in range(int(meta[0])):
		number = int(sys.stdin.readline())
		if number % divisor == 0:
			count += 1

	print(count)

if __name__ == '__main__':
	main()
