SPOJ solutions
==============

* [Profile](http://www.spoj.com/users/jmabey)

Classical
---------

1. ✔ [ADDREV](http://www.spoj.com/problems/ADDREV)
2. ✔ [ADV04L](http://www.spoj.com/problems/ADV04L)
3. ✔ [ARITH](http://www.spoj.com/problems/ARITH)
4. ✔ [FCTRL](http://www.spoj.com/problems/FCTRL)
5. ✔ [FCTRL2](http://www.spoj.com/problems/FCTRL2)
6. ✔ [JULKA](http://www.spoj.com/problems/JULKA)
7. ✔ [NSTEPS](http://www.spoj.com/problems/NSTEPS)
8. ✔ [ONP](http://www.spoj.com/problems/ONP)
9. ✘ [PALIN](http://www.spoj.com/problems/PALIN)
10. ✔ [PRIME1](http://www.spoj.com/problems/PRIME1)
11. ✔ [TEST](http://www.spoj.com/problems/TEST)

Challenge
---------

1. ✔ [KAMIL](http://www.spoj.com/problems/KAMIL)

Tutorial
--------

1. ✔ [BCEASY](http://www.spoj.com/problems/BCEASY)
2. ✘ [INTSS](http://www.spoj.com/problems/INTSS)
3. ✔ [INTEST](http://www.spoj.com/problems/INTEST)
4. ✘ [J4FUN](http://www.spoj.com/problems/J4FUN)
5. ✔ [LEXISORT](http://www.spoj.com/problems/LEXISORT)

Riddle
------

1. ✔ [LGIC](http://www.spoj.com/problems/LGIC)
