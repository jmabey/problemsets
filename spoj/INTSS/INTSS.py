"""
SPOJ INTSS solution
Python 3.1.2
Author: Jimmy Mabey
"""
import heapq
import sys

class Graph:
	# Unique identifier used in heaps for sorting purposes
	heap_count = 0

	def __init__(self, num_vertices):
		self.vertices = range(1, int(num_vertices) + 1)

		# Use 1-index to match input vertex numbers
		indexes = range(0, int(num_vertices) + 1)

		self.weights   = [0 for i in indexes]
		self.neighbors = [set() for i in indexes]

	def set_weight(self, vertex, weight):
		self.weights[int(vertex)] = int(weight)

	def add_edge(self, vertex1, vertex2):
		vertex1 = int(vertex1)
		vertex2 = int(vertex2)

		self.neighbors[vertex1].add(vertex2)
		self.neighbors[vertex2].add(vertex1)

	def max_iss_weight(self):
		"""
		Return the maximum weight of any given internally stable set within the
		graph.

		An internally stable set is basically a subset of the graph whose
		vertices are not neighbors of each other.
		"""
		# Track ISS's that might be subsets of larger ISS's
		unchecked = []
		heapq.heapify(unchecked)

		# Start with the trivial ISS's -- single vertices
		for i in self.vertices:
			self.push_heap_iss(unchecked, set([i]))

		best = max([self.iss_weight(set([i])) for i in self.vertices])

		# Heuristic search with the heuristic being largest theoretical weight
		while len(unchecked):
			(theoretical_best, iss) = self.pop_heap_iss(unchecked)

			if theoretical_best > best:
				# May have supersets that have a larger weight
				for superset in self.iss_supersets(iss):
					weight = self.iss_weight(superset)
					if weight > best:
						best = weight

					self.push_heap_iss(unchecked, superset)

		return best

	def push_heap_iss(self, heap, iss):
		"""Push an ISS onto a heap."""
		# Want to sort by theoretical best weight, but heaps are sorted by
		# smallest first, so store that value negative.  Also, a unique value is
		# needed in case weights are the same and because sets cannot be
		# compared numerically
		heapq.heappush(
			heap, (-self.theoretical_best(iss), self.heap_count, iss))
		self.heap_count += 1

	def pop_heap_iss(self, heap):
		"""Pop tuple (theoretical best weight, ISS) off of a heap."""
		(neg_theo_best, _, iss) = heapq.heappop(heap)
		return (-neg_theo_best, iss)

	def iss_supersets(self, iss):
		"""
		Return a list of ISS's that have the given ISS as a subset, assuming all
		vertices <= max(iss) have been checked before.
		"""
		neighbors = self.iss_neighbors(iss)
		remaining = [i for i in self.vertices if i > max(iss)]

		return [iss | set([i]) for i in remaining if i not in neighbors]

	def theoretical_best(self, subset):
		"""
		Given an internally stable subset, calculate the theoretical best weight
		of any superset.

		This is under the same assumption as iss_supersets() -- all vertices <=
		max(subset) have been checked before.
		"""
		neighbors = self.iss_neighbors(subset)
		remaining = set([
			i for i in self.vertices if i > max(subset) and i not in neighbors
		])

		return self.iss_weight(subset | remaining)

	def iss_neighbors(self, iss):
		neighbors = set()
		for i in iss:
			neighbors |= self.neighbors[i]
		return neighbors

	def iss_weight(self, iss):
		return sum(self.weights[i] for i in iss)

def main():
	for i in range(int(sys.stdin.readline())):
		num_vertices, num_edges = sys.stdin.readline().split()
		graph = Graph(num_vertices)

		for index, weight in enumerate(sys.stdin.readline().split()):
			graph.set_weight(index + 1, weight)

		for j in range(int(num_edges)):
			vertices = sys.stdin.readline().split()
			graph.add_edge(int(vertices[0]), int(vertices[1]))

		print(graph.max_iss_weight())

main()
