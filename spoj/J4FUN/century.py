import datetime

weekdays = set()

for year in range(100, 10000, 100):
	end = datetime.date(year, 12, 31)
	weekdays.add(end.weekday())

print(weekdays)
