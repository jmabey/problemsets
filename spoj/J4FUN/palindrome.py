import datetime

def is_palindrome(date):
	date_string = '{:02}{:02}{:04}'.format(date.month, date.day, date.year)
	return date_string == date_string[::-1]

def main():
	current = datetime.date(2001, 10, 1)

	while not is_palindrome(current):
		current -= datetime.timedelta(1)

	print(current)

if __name__ == '__main__':
	main()
