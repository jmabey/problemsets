for people in range(0, 100):
	on = people
	print('{} people:'.format(on))

	off = float(on) * 3 / 4
	on -= off
	on += 7
	print('  Stop A: {} got off, 7 get on, {} on bus'.format(off, on))

	off = float(on) * 3 / 4
	on -= off
	on += 7
	print('  Stop B: {} got off, 7 get on, {} on bus'.format(off, on))

	off = float(on) * 3 / 4
	print('  Stop C: {} got off'.format(off))

# 52 people were originally on.  At each stop, an integral number of people got
# off -- problem says it has to be exact, so you cannot have a fraction of
# a person get off.  52 is the only number this happens for, for each stop.
