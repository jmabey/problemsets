-- HackerRank: Weather Observation Station 4
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT COUNT(*) - COUNT(DISTINCT CITY) FROM STATION;
