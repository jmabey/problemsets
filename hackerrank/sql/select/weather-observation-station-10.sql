-- HackerRank: Weather Observation Station 10
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY FROM STATION WHERE CITY NOT REGEXP '[aeiou]$' ORDER BY CITY;
