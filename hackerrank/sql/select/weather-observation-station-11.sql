-- HackerRank: Weather Observation Station 11
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY
FROM STATION
WHERE CITY NOT REGEXP '^[AEIOU]' OR CITY NOT REGEXP '[aeiou]$'
ORDER BY CITY;
