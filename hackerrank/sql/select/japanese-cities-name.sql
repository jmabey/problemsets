-- HackerRank: Japanese Cities' Name
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT Name FROM City WHERE CountryCode = 'JPN';
