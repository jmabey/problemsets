-- HackerRank: Weather Observation Station 3
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY FROM STATION WHERE ID % 2 = 0 ORDER BY CITY;
