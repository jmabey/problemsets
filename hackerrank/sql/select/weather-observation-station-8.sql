-- HackerRank: Weather Observation Station 8
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY FROM STATION WHERE CITY REGEXP '^[AEIOU].*[aeiou]$' ORDER BY CITY;
