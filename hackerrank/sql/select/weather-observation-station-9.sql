-- HackerRank: Weather Observation Station 9
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY FROM STATION WHERE CITY NOT REGEXP '^[AEIOU]' ORDER BY CITY;
