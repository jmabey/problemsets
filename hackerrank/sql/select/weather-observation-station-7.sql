-- HackerRank: Weather Observation Station 7
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY FROM STATION WHERE CITY REGEXP '[aeiou]$' ORDER BY CITY;
