-- HackerRank: Weather Observation Station 12
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY
FROM STATION
WHERE CITY NOT REGEXP '^[AEIOU]' AND CITY NOT REGEXP '[aeiou]$'
ORDER BY CITY;
