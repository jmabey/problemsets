-- HackerRank: Revising the Select Query - 1
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT * FROM City WHERE CountryCode = 'USA' AND Population > 100000;
