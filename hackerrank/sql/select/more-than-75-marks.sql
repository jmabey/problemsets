-- HackerRank: Higher Than 75 Marks
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT Name FROM Students WHERE Marks > 75 ORDER BY RIGHT(Name, 3), ID;
