-- HackerRank: Revising the Select Query - 2
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT Name FROM City WHERE CountryCode = 'USA' AND Population > 120000;
