-- HackerRank: Weather Observation Station 1
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT CITY, STATE FROM STATION ORDER BY CITY, STATE;
