-- HackerRank: Weather Observation Station 6
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT DISTINCT CITY FROM STATION WHERE CITY REGEXP '^[AEIOU]' ORDER BY CITY;
