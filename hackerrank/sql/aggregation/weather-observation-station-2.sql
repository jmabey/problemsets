-- HackerRank: Weather Observation Station 2
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT CONCAT(ROUND(SUM(LAT_N), 2), ' ', ROUND(SUM(LONG_W), 2)) FROM STATION;
