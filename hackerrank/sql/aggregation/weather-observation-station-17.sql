-- HackerRank: Weather Observation Station 17
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT ROUND(LONG_W, 4) FROM STATION WHERE LAT_N > 38.778 ORDER BY LAT_N LIMIT 1;
