-- HackerRank: Weather Observation Station 15
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT ROUND(LONG_W, 4) FROM STATION WHERE LAT_N < 137.2345 ORDER BY LAT_N DESC LIMIT 1;
