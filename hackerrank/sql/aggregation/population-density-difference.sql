-- HackerRank: Population Density Difference
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT MAX(Population) - MIN(Population) FROM City;
