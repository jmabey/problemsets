-- HackerRank: The Blunder
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT CEIL(AVG(Salary) - AVG(REPLACE(Salary, '0', ''))) FROM Employees;
