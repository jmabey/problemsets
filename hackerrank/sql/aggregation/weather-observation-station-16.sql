-- HackerRank: Weather Observation Station 16
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT ROUND(MIN(LAT_N), 4) FROM STATION WHERE LAT_N > 38.778;
