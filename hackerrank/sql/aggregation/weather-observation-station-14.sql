-- HackerRank: Weather Observation Station 14
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT ROUND(MAX(LAT_N), 4) FROM STATION WHERE LAT_N < 137.2345;
