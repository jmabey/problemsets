-- HackerRank: Weather Observation Station 13
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT ROUND(SUM(LAT_N), 4) FROM STATION WHERE LAT_N > 38.788 AND LAT_N < 137.2345;
