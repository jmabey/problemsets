-- HackerRank: Average Population
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT FLOOR(AVG(Population)) FROM City;
