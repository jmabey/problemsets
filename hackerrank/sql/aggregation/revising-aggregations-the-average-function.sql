-- HackerRank: Revising Aggregations - Averages
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT AVG(Population) FROM City WHERE District = 'California';
