-- HackerRank: Weather Observation Station 20 
-- SQL (MySQL)
-- Author: Jimmy Mabey

SET @row = 0;

SELECT ROUND(LAT_N, 4)
FROM (SELECT @row := @row + 1 AS n, LAT_N FROM STATION ORDER BY LAT_N) AS r
WHERE n = (SELECT CEILING(COUNT(*) / 2) FROM STATION);
