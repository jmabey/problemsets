-- HackerRank: Revising Aggregations - The Sum function
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT SUM(Population) FROM City WHERE District = 'California';
