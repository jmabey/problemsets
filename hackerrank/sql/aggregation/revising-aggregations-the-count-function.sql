-- HackerRank: Revising Aggregations - The Count Function
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT COUNT(*) FROM City WHERE Population > 100000;
