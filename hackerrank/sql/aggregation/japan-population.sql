-- HackerRank: Japan Population
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT SUM(Population) FROM City WHERE CountryCode = 'JPN';
