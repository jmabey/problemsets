-- HackerRank: Placements
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT Name
FROM Students s
LEFT JOIN Packages sp ON s.ID = sp.ID
LEFT JOIN Friends f ON s.ID = f.ID
LEFT JOIN Packages fp ON f.Friend_ID = fp.ID
WHERE fp.Salary > sp.Salary
ORDER BY fp.Salary;
