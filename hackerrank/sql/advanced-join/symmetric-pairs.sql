-- HackerRank: Symmetric Pairs
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT f1.X, f1.Y
FROM Functions f1
JOIN Functions f2 ON f1.X = f2.Y AND f1.Y = f2.X
-- If (f1.X, f1.Y) and (f2.X, f2.Y) are symmetric pairs, this ensures only one
-- of the pairs is included.
WHERE f1.X <= f1.Y
GROUP BY f1.X, f1.Y
-- If X = Y, then there must be at least two rows because the pair by itself
-- isn't a symmetric pair.
HAVING COUNT(*) > 1 OR f1.X != f1.Y
ORDER BY f1.X;
