-- HackerRank: Projects
-- Go (Oracle)
-- Author: Jimmy Mabey

WITH numbered AS (
    SELECT ROW_NUMBER() OVER (ORDER BY End_Date) n, Start_Date, End_Date
    FROM Projects
)
SELECT MIN(Start_Date) s, MAX(End_Date) e
FROM numbered
GROUP BY End_Date - n
ORDER BY e - s, s;
