-- HackerRank: The Report
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT CASE WHEN Grade < 8 THEN NULL ELSE Name END, Grade, Marks
FROM Students
LEFT JOIN Grades ON Marks >= Min_Mark AND Marks <= Max_Mark
ORDER BY Grade DESC, Name;
