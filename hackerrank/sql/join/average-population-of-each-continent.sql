-- HackerRank: Average Population of Each Continent
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT Country.Continent, FLOOR(AVG(City.Population))
FROM City
JOIN Country ON City.CountryCode = Country.Code
GROUP BY Country.Continent;
