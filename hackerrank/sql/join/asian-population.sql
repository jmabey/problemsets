-- HackerRank: Asian Population
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT SUM(City.Population)
FROM City
LEFT JOIN Country ON City.CountryCode = Country.Code
WHERE Country.Continent = 'Asia';
