-- HackerRank: The PADS
-- SQL (MySQL)
-- Author: Jimmy Mabey

SELECT line
FROM (
    (
        SELECT 1 AS i, CONCAT(Name, '(', LEFT(Occupation, 1), ')') AS line
        FROM Occupations
    ) UNION ALL (
        SELECT 2 AS i, CONCAT('There are total ', COUNT(*), ' ', LOWER(Occupation), 's.') AS line
        FROM Occupations
        GROUP BY Occupation
    )
) AS u
ORDER BY i, line;
