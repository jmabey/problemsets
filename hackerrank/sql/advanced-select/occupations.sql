-- HackerRank: Occupations
-- SQL (Oracle)
-- Author: Jimmy Mabey

WITH numbered AS (
    SELECT ROW_NUMBER() OVER (PARTITION BY Occupation ORDER BY Name) n, o.*
    FROM Occupations o
)
SELECT d.Name, p.Name, s.Name, a.Name
FROM (SELECT * FROM numbered WHERE Occupation = 'Doctor' ORDER BY Name) d
FULL JOIN (SELECT * FROM numbered WHERE Occupation = 'Professor' ORDER BY Name) p ON d.n = p.n
FULL JOIN (SELECT * FROM numbered WHERE Occupation = 'Singer' ORDER BY Name) s ON p.n = s.n
FULL JOIN (SELECT * FROM numbered WHERE Occupation = 'Actor' ORDER BY Name) a ON s.n = a.n;
