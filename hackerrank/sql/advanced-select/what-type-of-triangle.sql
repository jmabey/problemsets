-- HackerRank: Type of Triangle
-- SQL (Oracle)
-- Author: Jimmy Mabey
--
-- Triangle inequality

SELECT CASE
    WHEN A + B <= C THEN 'Not A Triangle'
    WHEN A = B AND B = C THEN 'Equilateral'
    WHEN A = B OR A = C OR B = C THEN 'Isosceles'
    ELSE 'Scalene'
END
FROM Triangles;
