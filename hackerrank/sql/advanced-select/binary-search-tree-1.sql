-- HackerRank: Binary Search Tree
-- SQL (Oracle)
-- Author: Jimmy Mabey

SELECT
    b.N,
    CASE
        WHEN b.P IS NULL THEN 'Root'
        WHEN EXISTS (SELECT 1 FROM BST WHERE P = b.N) THEN 'Inner'
        ELSE 'Leaf'
    END
FROM BST b
ORDER BY b.N;
