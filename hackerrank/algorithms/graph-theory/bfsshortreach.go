// HackerRank: Breadth First Search: Shortest Reach
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"strings"
)

const (
	Distance    = 6
	Unreachable = -1
)

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n, e int
		fmt.Scanln(&n, &e)

		g := NewGraph(n)

		for j := 0; j < e; j++ {
			var x, y int
			fmt.Scanln(&x, &y)

			g.AddEdge(x, y)
		}

		var s int
		fmt.Scanln(&s)

		g.Measure(s)
		g.Println(s)
	}
}

type Graph struct {
	Nodes     int
	Distances []int

	// Edges[x][y] is true if an edge exists between nodes x and y.
	Edges [][]bool
}

func NewGraph(n int) *Graph {
	e := make([][]bool, n+1)
	for i := 1; i < len(e); i++ {
		e[i] = make([]bool, len(e))
	}

	d := make([]int, n+1)
	for i := 1; i < len(d); i++ {
		d[i] = Unreachable
	}

	return &Graph{Nodes: n, Edges: e, Distances: d}
}

func (g *Graph) AddEdge(x, y int) {
	g.Edges[x][y] = true
	g.Edges[y][x] = true
}

func (g *Graph) Measure(s int) {
	// Nodes to be visited are added to a queue, beginning with the starting
	// node.
	q := []int{s}

	// Distance is used to track whether or not a node has been visited. The
	// starting node's distance is zero.
	g.Distances[s] = 0

	for len(q) > 0 {
		// Dequeue the next node to visit.
		n := q[0]
		q = q[1:]

		// Queue all the unvisited, adjacent nodes of node n and calculate their
		// distance.
		for a := 1; a <= g.Nodes; a++ {
			if g.Edges[n][a] && g.Distances[a] == Unreachable {
				g.Distances[a] = g.Distances[n] + Distance
				q = append(q, a)
			}
		}
	}
}

func (g *Graph) Println(s int) {
	ds := make([]string, 0, g.Nodes-1)

	for n := 1; n <= g.Nodes; n++ {
		if n != s {
			ds = append(ds, fmt.Sprintf("%d", g.Distances[n]))
		}
	}

	fmt.Println(strings.Join(ds, " "))
}
