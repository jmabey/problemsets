// HackerRank: Jumping on the Clouds
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	tcs := make([]bool, n)
	for i := range tcs {
		var tc int
		fmt.Scan(&tc)
		tcs[i] = tc == 1
	}

	i, j := 0, 0
	for i < len(tcs)-1 {
		if i+2 < len(tcs) && !tcs[i+2] {
			i += 2
		} else {
			i++
		}
		j++
	}

	fmt.Println(j)
}
