// HackerRank: Sherlock and the Beast
// Go
// Author: Jimmy Mabey

// This is a brute-force solution. There is a far simpler, constant-time
// solution shown in the editorial for this problem.

package main

import (
	"fmt"
	"math/big"
	"strings"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n int
		fmt.Scanln(&n)

		dn := NewDecentNum(n)
		dn.FindLargest()
		dn.Println()
	}
}

type DecentNum struct {
	Digits int

	// A Decent Number only has 3 or 5 as digits, so we can treat it like
	// a binary number where 0 represents 3 and 1 represents 5.
	Num big.Int
}

func NewDecentNum(n int) *DecentNum {
	// Start with the largest possible number with n digits.
	dn := &DecentNum{Digits: n}
	dn.Num.SetString(strings.Repeat("1", n), 2)
	return dn
}

func (dn *DecentNum) FindLargest() {
	// As a binary number, the next number to try is found by subtracting one.
	// The search stops at zero or the first valid number.
	for dn.Num.BitLen() != 0 && !dn.Valid() {
		dn.Num.Sub(&dn.Num, big.NewInt(1))
	}
}

func (dn *DecentNum) Valid() bool {
	// [number of 3s, number of 5s]
	var c [2]int
	for i := 0; i < dn.Digits; i++ {
		c[dn.Num.Bit(i)]++
	}

	// Number of 3s divisible by 5 and number of 5s divisible by 3.
	return c[0]%5 == 0 && c[1]%3 == 0
}

func (dn *DecentNum) Println() {
	if dn.Valid() {
		// Replace binary digits with 3s and 5s. Also need padding so that
		// leading 0s are included.
		f := fmt.Sprintf("%%0%db", dn.Digits)
		s := fmt.Sprintf(f, &dn.Num)
		s = strings.Replace(s, "0", "3", -1)
		s = strings.Replace(s, "1", "5", -1)
		fmt.Println(s)
	} else {
		fmt.Println(-1)
	}
}
