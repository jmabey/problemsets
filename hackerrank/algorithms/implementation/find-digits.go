// HackerRank: Find Digits
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func sliceInt(n int) []int {
	var ds []int
	for n > 0 {
		ds = append(ds, n%10)
		n /= 10
	}
	return ds
}

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n int
		fmt.Scanln(&n)

		c := 0
		ds := sliceInt(n)
		for j := 0; j < len(ds); j++ {
			if ds[j] != 0 && n%ds[j] == 0 {
				c++
			}
		}

		fmt.Println(c)
	}
}
