// HackerRank: Bigger is Greater
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"sort"
	"strings"
)

// NoAnswer is returned when there is no string that is lexicographically
// greater than the given string.
const NoAnswer = "no answer"

// Next finds the smallest string lexicographically larger than s by rearranging
// the letters in s. If no such string exists, NoAnswer is returned.
func Next(s string) string {
	ss := strings.Split(s, "")

	// Scan the string from right to left. For each letter i, find the smallest
	// letter j after letter i that is greater than letter i. If letter
	// j exists, then the search is stopped. Swapping letters i and j and then
	// sorting the letters after position i will create the smallest string
	// greater than s.
	sortAfter, swapWith := -1, -1
	for i := len(s) - 1; i >= 0; i-- {
		for j := i + 1; j < len(s); j++ {
			if s[j] > s[i] && (swapWith == -1 || s[j] < s[swapWith]) {
				swapWith = j
			}
		}
		if swapWith != -1 {
			sortAfter = i
			break
		}
	}
	if sortAfter == -1 {
		return NoAnswer
	}

	ss[sortAfter], ss[swapWith] = ss[swapWith], ss[sortAfter]
	sort.Strings(ss[sortAfter+1:])
	return strings.Join(ss, "")
}

func main() {
	var n int
	fmt.Scanln(&n)

	for i := 0; i < n; i++ {
		var s string
		fmt.Scanln(&s)
		fmt.Println(Next(s))
	}
}
