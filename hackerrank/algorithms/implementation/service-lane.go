// HackerRank: Service Lane
// Go
// Author: Jimmy Mabey

package main

import "fmt"

// Possible widths for all lane segments.
const (
	MinWidth = 1
	MaxWidth = 3
)

func main() {
	var n, t int
	fmt.Scanln(&n, &t)

	w := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&w[i])
	}

	for k := 0; k < t; k++ {
		var i, j int
		fmt.Scanln(&i, &j)

		min := MaxWidth
		for ; i <= j; i++ {
			if w[i] < min {
				min = w[i]
			}
			if min == MinWidth {
				break
			}
		}

		fmt.Println(min)
	}
}
