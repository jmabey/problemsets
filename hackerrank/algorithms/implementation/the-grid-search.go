// HackerRank: The Grid Search
// Go
// Author: Jimmy Mabey

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var t int
	fmt.Fscanln(in, &t)

	for i := 0; i < t; i++ {
		var gr, gc int
		fmt.Fscanln(in, &gr, &gc)

		g := make([][]byte, gr)
		for j := range g {
			fmt.Fscanln(in, &g[j])
		}

		var pr, pc int
		fmt.Fscanln(in, &pr, &pc)

		p := make([][]byte, pr)
		for j := range p {
			fmt.Fscanln(in, &p[j])
		}

		if Search(g, p) {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}

// Search returns true if pattern p is found in grid g.
func Search(g, p [][]byte) bool {
	for i := 0; i <= len(g)-len(p); i++ {
	NextByte:
		for j := 0; j <= len(g[0])-len(p[0]); j++ {
			for k := 0; k < len(p); k++ {
				for l := 0; l < len(p[k]); l++ {
					if g[i+k][j+l] != p[k][l] {
						continue NextByte
					}
				}
			}

			return true
		}
	}

	return false
}

/*
	Previous attempts:

	1. Without using bufio, reading input was taking >1s for large test cases.
*/
