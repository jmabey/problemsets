// HackerRank: Encryption
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math"
)

func main() {
	var in string
	fmt.Scanln(&in)

	c := int(math.Sqrt(float64(len(in))))
	if c*c < len(in) {
		c++
	}

	out := []byte{}
	for i := 0; i < c; i++ {
		for j := i; j < len(in); j += c {
			out = append(out, in[j])
		}
		if i < c-1 {
			out = append(out, ' ')
		}
	}

	fmt.Printf("%s\n", out)
}
