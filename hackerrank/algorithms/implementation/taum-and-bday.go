// HackerRank: Taum and B'day
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var b, w, x, y, z int
		fmt.Scanln(&b, &w)
		fmt.Scanln(&x, &y, &z)

		c := 0
		if y+z < x {
			c += b * (y + z)
		} else {
			c += b * x
		}
		if x+z < y {
			c += w * (x + z)
		} else {
			c += w * y
		}

		fmt.Println(c)
	}
}
