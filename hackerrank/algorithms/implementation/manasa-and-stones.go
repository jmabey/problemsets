// HackerRank: Manasa and Stones
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n, a, b int
		fmt.Scanln(&n)
		fmt.Scanln(&a)
		fmt.Scanln(&b)

		set := map[int]bool{}
		for i := 0; i < n; i++ {
			set[(n-i-1)*a+i*b] = true
		}

		l := []int{}
		for i := range set {
			l = append(l, i)
		}
		sort.Ints(l)

		fmt.Println(strings.Trim(fmt.Sprintf("%d", l), "[]"))
	}
}

/*
	Recursive solution (too slow):

	// Trail represents a trail of stones with state to guess the values of the
	// last stones in the trail.
	type Trail struct {
		Len   int
		Diffs []int
		Last  map[int]bool
	}

	// NewTrail initializes a new trial of length n with ds as the possible
	// differences between stones.
	func NewTrail(n int, ds []int) *Trail {
		return &Trail{
			Len:   n,
			Diffs: ds,
			Last:  map[int]bool{},
		}
	}

	// PossibleLast returns a slice of possible values for the last stone in
	// increasing order.
	func (s Trail) PossibleLast() []int {
		s.recurse(s.Len-1, 0)

		var vs []int
		for v := range s.Last {
			vs = append(vs, v)
		}
		sort.Ints(vs)

		return vs
	}

	func (s Trail) recurse(l, v int) {
		if l <= 0 {
			s.Last[v] = true
			return
		}

		for _, d := range s.Diffs {
			s.recurse(l-1, v+d)
		}
	}
*/
