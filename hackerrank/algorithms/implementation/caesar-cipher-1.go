// HackerRank: Caesar Cipher
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n, k int
	var s []byte
	fmt.Scanln(&n)
	fmt.Scanln(&s)
	fmt.Scanln(&k)

	for i := range s {
		if s[i] >= 'A' && s[i] <= 'Z' {
			s[i] = 'A' + byte((int(s[i]-'A')+k)%26)
		} else if s[i] >= 'a' && s[i] <= 'z' {
			s[i] = 'a' + byte((int(s[i]-'a')+k)%26)
		}
	}
	fmt.Printf("%s\n", s)
}
