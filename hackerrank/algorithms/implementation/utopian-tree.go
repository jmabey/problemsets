// HackerRank: Utopian Tree
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var c int
		fmt.Scanln(&c)

		fmt.Println(height(c))
	}
}

func height(c int) int {
	// 1, 2, 3, 6, 7, 14, 15, ... There is a formula for this sequence:
	// https://oeis.org/A075427
	cf := float64(c)
	return int(math.Pow(2, math.Floor((cf+3)/2)) - 3/2 + math.Pow(-1, cf)/2)
}
