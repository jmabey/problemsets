// HackerRank: The Time in Words
// Go
// Author: Jimmy Mabey

package main

import "fmt"

// Words maps numbers to their spelled-out strings.
var Words = map[int]string{
	1:  "one",
	2:  "two",
	3:  "three",
	4:  "four",
	5:  "five",
	6:  "six",
	7:  "seven",
	8:  "eight",
	9:  "nine",
	10: "ten",
	11: "eleven",
	12: "twelve",
	13: "thirteen",
	14: "fourteen",
	15: "fifteen",
	16: "sixteen",
	17: "seventeen",
	18: "eighteen",
	19: "nineteen",
	20: "twenty",
	21: "twenty one",
	22: "twenty two",
	23: "twenty three",
	24: "twenty four",
	25: "twenty five",
	26: "twenty six",
	27: "twenty seven",
	28: "twenty eight",
	29: "twenty nine",
}

func main() {
	var h, m int
	fmt.Scanln(&h)
	fmt.Scanln(&m)

	switch {
	case m == 0:
		fmt.Println(Words[h], "o' clock")
	case m == 1:
		fmt.Println(Words[m], "minute past", Words[h])
	case m == 15:
		fmt.Println("quarter past", Words[h])
	case m == 30:
		fmt.Println("half past", Words[h])
	case m == 45:
		fmt.Println("quarter to", Words[h+1])
	case m == 59:
		fmt.Println(Words[60-m], "minute to", Words[h+1])
	case m < 30:
		fmt.Println(Words[m], "minutes past", Words[h])
	default:
		fmt.Println(Words[60-m], "minutes to", Words[h+1])
	}
}
