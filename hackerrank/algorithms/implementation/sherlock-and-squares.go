// HackerRank: Sherlock and Squares
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var a, b float64
		fmt.Scanln(&a, &b)

		// ceil(sqrt(a)) is the smallest number when squared is > a.
		// floor(sqrt(b) is the larger number when squared is < b.
		// Thus counting the numbers between these (inclusive) is the number of
		// square integers between a and b.
		c := int(math.Floor(math.Sqrt(b))-math.Ceil(math.Sqrt(a))) + 1
		if c < 0 {
			c = 0
		}

		fmt.Println(c)
	}
}

/*
	Brute-force:

	c := 0
	for j := a; j <= b; j++ {
		sr := math.Sqrt(float64(j))
		if sr == math.Floor(sr) {
			c++
		}
	}
*/
