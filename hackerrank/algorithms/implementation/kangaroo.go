// HackerRank: Kangaroo
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math"
)

func main() {
	var x1, v1, x2, v2 float64
	fmt.Scanln(&x1, &v1, &x2, &v2)

	// x1 + j*v1 = x2 + j*v2, solve for j, the number of jumps.
	// Must also be a whole number of jumps.
	j := (x1 - x2) / (v2 - v1)
	if j > 0 && j == math.Trunc(j) {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
