// HackerRank: Cavity Map
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	m := make([][]byte, n)
	for i := range m {
		fmt.Scanln(&m[i])
	}

	for i := 1; i < len(m)-1; i++ {
		for j := 1; j < len(m[i])-1; j++ {
			c := m[i][j]
			if m[i-1][j] < c && m[i+1][j] < c && m[i][j-1] < c && m[i][j+1] < c {
				m[i][j] = 'X'
			}
		}
	}

	for i := range m {
		fmt.Printf("%s\n", m[i])
	}
}
