// HackerRank: Modified Kaprekar Numbers
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"strings"
)

func main() {
	var p, q int
	fmt.Scanln(&p)
	fmt.Scanln(&q)

	kns := []int{}
	for i := p; i <= q; i++ {
		var (
			s   = IntToSlice(i * i)
			mid = len(s)/2 + len(s)%2
			l   = SliceToInt(s[mid:])
			r   = SliceToInt(s[:mid])
		)

		if l+r == i {
			kns = append(kns, i)
		}
	}

	if len(kns) > 0 {
		fmt.Println(strings.Trim(fmt.Sprintf("%d", kns), "[]"))
	} else {
		fmt.Println("INVALID RANGE")
	}
}

// IntToSlice converts n into a slice (lowest place value first) containing its
// digits.
func IntToSlice(n int) []int {
	var ds []int
	for n > 0 {
		ds = append(ds, n%10)
		n /= 10
	}
	return ds
}

// SliceToInt converts s, representing the digits of a number (lowest place
// value first), into an int.
func SliceToInt(s []int) int {
	n := 0
	for i := len(s) - 1; i >= 0; i-- {
		n = n*10 + s[i]
	}
	return n
}
