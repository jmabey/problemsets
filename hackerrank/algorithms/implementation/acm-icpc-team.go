// HackerRank: ACM ICPC Team
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math/big"
)

func main() {
	var n, m int
	fmt.Scanln(&n, &m)

	ts := make([]big.Int, n)
	for i := range ts {
		var s string
		fmt.Scanln(&s)
		ts[i].SetString(s, 2)
	}

	cs := map[int]int{}
	max := 0
	for i := range ts {
		for j := i + 1; j < len(ts); j++ {
			t := big.NewInt(0).Or(&ts[i], &ts[j])

			c := 0
			for k := 0; k < m; k++ {
				c += int(t.Bit(k))
			}

			cs[c]++

			if c > max {
				max = c
			}
		}
	}

	fmt.Println(max)
	fmt.Println(cs[max])
}
