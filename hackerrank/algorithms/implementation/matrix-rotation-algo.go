// HackerRank: [Algo] Matrix Rotation
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"strings"
)

func main() {
	var m, n, r int
	fmt.Scanln(&m, &n, &r)

	mtx := make([][]int, m)
	for i := range mtx {
		mtx[i] = make([]int, n)
		for j := range mtx[i] {
			fmt.Scan(&mtx[i][j])
		}
	}

	bs := Unbox(mtx)
	bs = Rotate(bs, r)
	mtx = Box(m, n, bs)

	for _, r := range mtx {
		fmt.Println(strings.Trim(fmt.Sprintf("%d", r), "[]"))
	}
}

// Unbox returns a 2D slice containing the values of each rectangle inside the
// matrix mtx in clockwise order. For example:
//
//     [[1  2  3  4  5],
//      [14 15 16 17 6],  returns:  [[1 2 3 4 5 6 7 8 9 10 11 12 13 14],
//      [13 20 19 18 7],             [15, 16, 17, 18, 19, 20]]
//      [12 11 10 9  8]]
func Unbox(mtx [][]int) [][]int {
	bs := [][]int{}

	for i := 0; i < len(mtx)/2 && i < len(mtx[i])/2; i++ {
		b := []int{}
		br, rc := RectCorner(i, len(mtx), len(mtx[i]))

		// Copy values from mtx to b, clockwise in the rectangle starting at
		// (i, i).
		for j := i; j <= rc; j++ {
			b = append(b, mtx[i][j])
		}
		for j := i + 1; j < br; j++ {
			b = append(b, mtx[j][rc])
		}
		for j := rc; j >= i; j-- {
			b = append(b, mtx[br][j])
		}
		for j := br - 1; j > i; j-- {
			b = append(b, mtx[j][i])
		}

		bs = append(bs, b)
	}

	return bs
}

// Rotate rotates the values in an unboxed matrix bs counterclockwise by
// s shifts.
func Rotate(bs [][]int, s int) [][]int {
	rbs := make([][]int, len(bs))
	for i, b := range bs {
		rbs[i] = make([]int, len(b))
		for j := range b {
			rbs[i][j] = b[(j+s)%len(b)]
		}
	}
	return rbs
}

// Box returns an m by n matrix using the 2D slice bs returned by Unbox.
func Box(m, n int, bs [][]int) [][]int {
	mtx := make([][]int, m)
	for i := range mtx {
		mtx[i] = make([]int, n)
	}

	for i, b := range bs {
		br, rc := RectCorner(i, m, n)

		// Copy values from b to mtx, clockwise in the rectangle starting at
		// (i, i).
		k := 0
		for j := i; j <= rc; j, k = j+1, k+1 {
			mtx[i][j] = b[k]
		}
		for j := i + 1; j < br; j, k = j+1, k+1 {
			mtx[j][rc] = b[k]
		}
		for j := rc; j >= i; j, k = j-1, k+1 {
			mtx[br][j] = b[k]
		}
		for j := br - 1; j > i; j, k = j-1, k+1 {
			mtx[j][i] = b[k]
		}
	}

	return mtx
}

// RectCorner returns the bottom-right point of a rectangle in an m by
// n matrix, with the rectangle's top-left point existing at (i, i).
func RectCorner(i, m, n int) (int, int) {
	return m - i - 1, n - i - 1
}
