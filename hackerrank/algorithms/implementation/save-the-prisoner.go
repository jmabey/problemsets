// HackerRank: Save the Prisoner
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n, m, s int
		fmt.Scanln(&n, &m, &s)

		// IDs are 1-indexed (s-1) and a candy is given to the starting ID
		// (so m-1 gives the last ID).
		fmt.Println((s+m-2)%n + 1)
	}
}
