// HackerRank: Divisible Sum Pairs
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n, k int
	fmt.Scanln(&n, &k)

	a := make([]int, n)
	for i := range a {
		fmt.Scan(&a[i])
	}

	p := 0
	for i := range a {
		for j := i + 1; j < len(a); j++ {
			if (a[i]+a[j])%k == 0 {
				p++
			}
		}
	}

	fmt.Println(p)
}
