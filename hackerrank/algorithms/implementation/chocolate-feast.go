// HackerRank: Chocolate Feast
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n, c, m int
		fmt.Scanln(&n, &c, &m)

		eaten := n / c
		wraps := eaten

		for wraps >= m {
			trades := wraps / m
			wraps = wraps % m

			eaten += trades
			wraps += trades
		}

		fmt.Println(eaten)
	}
}
