// HackerRank: Angry Professor
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n, k int
		fmt.Scanln(&n, &k)

		var b int
		for j := 0; j < n; j++ {
			var a int
			fmt.Scan(&a)

			if a <= 0 {
				b++
			}
		}

		if b < k {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
