// HackerRank: Non-Divisible Subset
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n, k int
	fmt.Scanln(&n, &k)

	// Count the occurrences of each possible remainder v%k for each input
	// value v.
	c := make([]int, k)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		c[v%k]++
	}

	s := 0

	// Each non-zero remainder r can be paired with k-r. That is, r + k-r = k
	// and thus the pair has a sum divisible by k. So only r or k-r can be in
	// the non-divisible subset. So pick the one with the larger count.
	for r := 1; r <= (k-1)/2; r++ {
		if c[r] > c[k-r] {
			s += c[r]
		} else {
			s += c[k-r]
		}
	}

	// Special case: if a value is divisible k (remainder of zero), then only
	// one of those values can be in the non-divisible subset.
	if c[0] > 0 {
		s++
	}

	// Special case: if k is even, then only one value with remainder k/2 can
	// be in the non-divisible subset.
	if k%2 == 0 && c[k/2] > 0 {
		s++
	}

	fmt.Println(s)
}
