// HackerRank: Cut the sticks
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	fmt.Scanln(&n)

	s := make([]int, n)
	for i := range s {
		fmt.Scan(&s[i])
	}
	sort.Ints(s)

	for len(s) > 0 {
		fmt.Println(len(s))

		first := 0
		min := s[0]
		for i := range s {
			s[i] -= min
			if s[i] <= 0 {
				first = i + 1
			}
		}
		s = s[first:]
	}
}
