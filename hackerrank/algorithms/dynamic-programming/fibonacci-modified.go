// HackerRank: Fibonacci Modified
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math/big"
)

func main() {
	var a, b big.Int
	var n int
	fmt.Scan(&a, &b, &n)

	for i := 0; i < n-2; i++ {
		var next big.Int
		next.Mul(&b, &b)
		next.Add(&next, &a)

		a = b
		b = next
	}

	fmt.Println(b.String())
}
