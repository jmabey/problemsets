// HackerRank: Alternating Characters
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var s string
		fmt.Scanln(&s)

		var (
			prev rune
			c    = 0
		)
		for _, r := range s {
			if r == prev {
				c++
			}
			prev = r
		}

		fmt.Println(c)
	}
}
