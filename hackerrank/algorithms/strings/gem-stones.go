// HackerRank: Gemstones
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	counts := map[rune]int{}
	for i := 0; i < n; i++ {
		var s string
		fmt.Scanln(&s)

		exists := map[rune]bool{}
		for _, ch := range s {
			exists[ch] = true
		}

		for ch := range exists {
			counts[ch]++
		}
	}

	g := 0
	for _, c := range counts {
		if c == n {
			g++
		}
	}

	fmt.Println(g)
}
