// HackerRank: Game of Thrones - I
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var s string
	fmt.Scanln(&s)

	count := map[rune]int{}
	for _, ch := range s {
		count[ch]++
	}

	// Count the number of characters with an odd number of occurrences in the
	// string. Odd-length strings must have exactly one odd character,
	// even-length must have none.
	odd := 0
	for _, n := range count {
		if n%2 == 1 {
			odd++
		}
	}

	if odd == len(s)%2 {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
