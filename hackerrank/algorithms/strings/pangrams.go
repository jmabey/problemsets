// HackerRank: Pangrams
// Go
// Author: Jimmy Mabey

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	in := bufio.NewScanner(os.Stdin)
	in.Scan()
	s := strings.ToLower(in.Text())

	c := map[rune]int{}
	for _, ch := range s {
		if ch >= 'a' && ch <= 'z' {
			c[ch]++
		}
	}

	if len(c) == 26 {
		fmt.Println("pangram")
	} else {
		fmt.Println("not pangram")
	}
}
