// HackerRank: Funny String
// Go
// Author: Jimmy Mabey

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var t int
	fmt.Scanln(&t)

	in := bufio.NewScanner(os.Stdin)
	for i := 0; i < t; i++ {
		in.Scan()
		s := in.Text()

		f := true
		for i := 1; i < len(s); i++ {
			sd := int(s[i]) - int(s[i-1])
			if sd < 0 {
				sd *= -1
			}

			rd := int(s[len(s)-i]) - int(s[len(s)-i-1])
			if rd < 0 {
				rd *= -1
			}

			if sd != rd {
				f = false
				break
			}
		}

		if f {
			fmt.Println("Funny")
		} else {
			fmt.Println("Not Funny")
		}
	}
}
