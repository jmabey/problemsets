// HackerRank: Compare the Triplets
// Go
// Author: Jimmy Mabey

package main

import "fmt"

// NumRatings is the number of ratings per person.
const NumRatings = 3

func main() {
	a := make([]int, NumRatings)
	b := make([]int, NumRatings)
	for i := range a {
		fmt.Scanf("%d", &a[i])
	}
	for i := range b {
		fmt.Scanf("%d", &b[i])
	}

	var as, bs int
	for i := range a {
		switch {
		case a[i] > b[i]:
			as++
		case a[i] < b[i]:
			bs++
		}
	}

	fmt.Println(as, bs)
}
