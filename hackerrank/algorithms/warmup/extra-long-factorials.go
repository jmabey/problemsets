// HackerRank: Extra long factorials
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math/big"
)

func main() {
	var n int64
	fmt.Scanln(&n)

	var f big.Int
	f.MulRange(1, n)

	fmt.Println(f.String())
}
