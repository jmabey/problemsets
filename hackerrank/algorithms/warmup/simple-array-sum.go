// HackerRank: Simple Array Sum
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n int
	fmt.Scan(&n)

	sum := 0
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		sum += v
	}

	fmt.Println(sum)
}
