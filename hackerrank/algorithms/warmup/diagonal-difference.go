// HackerRank: Diagonal Difference
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n int
	fmt.Scan(&n)

	tlbr, trbl := 0, 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			var v int
			fmt.Scan(&v)

			if i == j {
				tlbr += v
			}
			if n-i-1 == j {
				trbl += v
			}
		}
	}

	diff := tlbr - trbl
	if diff < 0 {
		diff = -diff
	}

	fmt.Println(diff)
}
