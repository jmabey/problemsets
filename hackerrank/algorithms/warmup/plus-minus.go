// HackerRank: Plus Minus
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func divide(t, n int) float32 {
	return float32(t) / float32(n)
}

func main() {
	var n int
	fmt.Scan(&n)

	var tn, tz, tp int
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)

		if v > 0 {
			tp++
		} else if v < 0 {
			tn++
		} else {
			tz++
		}
	}

	fmt.Printf("%.3f\n%.3f\n%.3f", divide(tp, n), divide(tn, n), divide(tz, n))
}
