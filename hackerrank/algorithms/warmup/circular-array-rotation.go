// HackerRank: Circular Array Rotation
// Go
// Author: Jimmy Mabey

package main

import "fmt"

// RotatableIntSlice represents a slice that is transformed by right rotation
// operations.
type RotatableIntSlice struct {
	Slice    []int
	Rotation int
}

// NewRotatableIntSlice initializes a RotatableIntSlice with size s and a right
// rotation of r.
func NewRotatableIntSlice(s, r int) *RotatableIntSlice {
	return &RotatableIntSlice{Slice: make([]int, s), Rotation: r}
}

// Get returns the ith value in the rotated slice.
func (ris RotatableIntSlice) Get(i int) int {
	i = (i - ris.Rotation) % len(ris.Slice)
	if i < 0 {
		i += len(ris.Slice)
	}
	return ris.Slice[i]
}

func main() {
	var s, r, q int
	fmt.Scanln(&s, &r, &q)

	ris := NewRotatableIntSlice(s, r)
	for i := range ris.Slice {
		fmt.Scan(&ris.Slice[i])
	}

	for i := 0; i < q; i++ {
		var j int
		fmt.Scanln(&j)
		fmt.Println(ris.Get(j))
	}
}
