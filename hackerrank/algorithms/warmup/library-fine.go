// HackerRank: Library Fine
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var ad, am, ay int
	fmt.Scanln(&ad, &am, &ay)

	var ed, em, ey int
	fmt.Scanln(&ed, &em, &ey)

	var f int
	if ay > ey {
		f = 10000
	} else if ay == ey && am > em {
		f = 500 * (am - em)
	} else if ay == ey && am == em && ad > ed {
		f = 15 * (ad - ed)
	}

	fmt.Println(f)
}
