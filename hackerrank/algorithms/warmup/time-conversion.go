// HackerRank: Time Conversion
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var h, m, s int
	var ampm string
	fmt.Scanf("%d:%d:%d%s", &h, &m, &s, &ampm)

	if h == 12 && ampm == "AM" {
		h = 0
	} else if h != 12 && ampm == "PM" {
		h += 12
	}

	fmt.Printf("%02d:%02d:%02d", h, m, s)
}
