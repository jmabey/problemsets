// HackerRank: Solve me first
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var a, b int
	fmt.Scan(&a, &b)
	fmt.Println(a + b)
}
