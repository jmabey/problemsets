// HackerRank: Insertion Sort - Part 1
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"strings"
)

func printArray(ar []int) {
	fmt.Println(strings.Trim(fmt.Sprintf("%d", ar), "[]"))
}

func main() {
	var s int
	fmt.Scanln(&s)

	ar := make([]int, s)
	for i := 0; i < s; i++ {
		fmt.Scan(&ar[i])
	}

	v := ar[s-1]
	for i := s - 1; i >= 0; i-- {
		// End of array or previous element is smaller: found where to insert v.
		if i == 0 || ar[i-1] <= v {
			ar[i] = v
			break
		}

		// Previous element is larger: copy previous value to "empty" slot.
		ar[i] = ar[i-1]
		printArray(ar)
	}

	printArray(ar)
}
