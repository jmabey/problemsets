// HackerRank: Intro to Tutorial Challenges
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var v int
	fmt.Scanln(&v)

	var n int
	fmt.Scanln(&n)

	for i := 0; i < n; i++ {
		var av int
		fmt.Scan(&av)

		if av == v {
			fmt.Println(i)
			break
		}
	}
}
