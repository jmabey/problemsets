// HackerRank: Points
// Go
// Author: Jimmy Mabey
//
// This is a convex hull problem, except the convex hull has to be aligned to
// a grid and must not place any of the input points along the border of the
// convex hull.
//
// 1. For each input point, add the four closest points surrounding the input
//    point. This forces the algorithm to place the convex hull around but not
//    on top of the input points.
//
// 2. Use the monotone chain algorithm to determine the convex hull.
//
// 3. Insert points so that all edges of the convex hull are aligned to the
//    sides and diagonals of the grid.
//
// Resources:
//
// https://en.wikipedia.org/wiki/Convex_hull_algorithms
// https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
// http://tomswitzer.net/2010/03/graham-scan/

package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	var n int
	fmt.Scanln(&n)

	ch := &ConvexHull{}
	for i := 0; i < n; i++ {
		var x, y int
		fmt.Scanln(&x, &y)

		ch.Add(x, y)
	}

	ch.Find()
	ch.Align()
	fmt.Printf("%.3f\n", ch.Perimeter())
}

// Point is a point in 2D space.
type Point struct {
	x, y int
}

// LeftOf returns true if p is found to the left (or counterclockwise) of the
// line formed by p1 and p2.
func (p Point) LeftOf(p1, p2 Point) bool {
	// See: https://en.wikipedia.org/wiki/Curve_orientation
	return (p2.x-p1.x)*(p.y-p1.y)-(p.x-p1.x)*(p2.y-p1.y) > 0
}

// Dist returns the distance of the line between p and q.
func (p Point) Dist(q Point) float64 {
	// See: http://www.purplemath.com/modules/distform.htm
	dx := q.x - p.x
	dy := q.y - p.y
	return math.Sqrt(float64(dx*dx + dy*dy))
}

// Points is a slice of points that implements sort.Interface, sorting by
// smallest x then smallest y.
type Points []Point

func (ps Points) Len() int {
	return len(ps)
}

func (ps Points) Swap(i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}

func (ps Points) Less(i, j int) bool {
	return ps[i].x < ps[j].x || (ps[i].x == ps[j].x && ps[i].y < ps[j].y)
}

// After returns the point after point i in this slice, wrapping around to the
// first point if point i is the last point.
func (ps Points) After(i int) Point {
	return ps[(i+1)%len(ps)]
}

// Insert inserts p after point i and returns the updated slice.
func (ps Points) Insert(p Point, i int) Points {
	ps = append(ps, Point{})
	copy(ps[i+2:], ps[i+1:])
	ps[i+1] = p
	return ps
}

// Pop returns this slice with the last point removed.
func (ps Points) Pop() Points {
	return ps[:len(ps)-1]
}

// AddHull implements a step in the monotone chain algorithm that adds p to ps
// and ensures that p and the previous two points in ps form a counterclockwise
// turn. The updated slice is returned.
func (ps Points) AddHull(p Point) Points {
	for len(ps) >= 2 && !p.LeftOf(ps[len(ps)-2], ps[len(ps)-1]) {
		ps = ps.Pop()
	}
	return append(ps, p)
}

// ConvexHull stores an input set of points and provides a way to find a convex
// hull surrounding those points.
type ConvexHull struct {
	Points Points
	Hull   Points
}

// Add adds a point to the input set of points.
func (ch *ConvexHull) Add(x, y int) {
	// The smallest polygon that can contain the point is a diamond:
	//
	//     x
	//    / \
	//   x P x
	//    \ /
	//     x
	//
	// Since we don't want any points on the border of the convex hull, we can
	// just add the four points of this diamond to force the convex hull to
	// contain the diamond instead of the point itself.

	ch.Points = append(ch.Points, Point{x + 1, y}, Point{x - 1, y}, Point{x, y + 1}, Point{x, y - 1})
}

// Find finds and stores the points of a convex hull surrounding the input set
// of points.
func (ch *ConvexHull) Find() {
	sort.Sort(ch.Points)

	l := Points{}
	for _, p := range ch.Points {
		l = l.AddHull(p)
	}

	u := Points{}
	for i := len(ch.Points) - 1; i >= 0; i-- {
		u = u.AddHull(ch.Points[i])
	}

	l = l.Pop()
	u = u.Pop()

	ch.Hull = l
	ch.Hull = append(ch.Hull, u...)
}

// Align adjusts the convex hull to align to a grid by inserting points so that
// each edge runs along a side or diagonal of the grid cells.
func (ch *ConvexHull) Align() {
	// Basic idea here is to convert each non-diagonal, non-side edge into
	// a diagonal edge and a side edge. This doesn't accurately approximate the
	// slope of the original edge and the convex hull may not be a valid hull
	// anymore. This doesn't matter because all we care about is the perimeter
	// and it stays the same no matter how we align the edges.

	// ch.Hull is modified in-place.
	for i := 0; i < len(ch.Hull); i++ {
		p := ch.Hull[i]
		q := ch.Hull.After(i)

		dx := q.x - p.x
		dy := q.y - p.y

		// Ignore points already aligned to sides or diagonals.
		if dx == 0 || dy == 0 || dx == dy || -dx == dy {
			continue
		}

		// The x and y directions the edge is moving towards.
		xdir, ydir := 1, 1
		if q.x < p.x {
			xdir = -1
		}
		if q.y < p.y {
			ydir = -1
		}

		// Need to insert the longest possible diagonal edge. Take the smallest
		// absolute value out of dx and dy (multiplying by xdir or ydir gives
		// the absolute value), then add a point to create an edge with an
		// absolute slope of 1.
		//
		// The edge from dp to q becomes a vertical or horizontal edge.
		d := xdir * dx
		if ady := ydir * dy; ady < d {
			d = ady
		}

		dp := Point{p.x + xdir*d, p.y + ydir*d}
		ch.Hull = ch.Hull.Insert(dp, i)

		// Skip over dp, since the next edge is between dp and q and we know
		// that is aligned.
		i++
	}
}

// Perimeter returns the perimeter of the convex hull.
func (ch ConvexHull) Perimeter() float64 {
	per := 0.0
	for i, p := range ch.Hull {
		per += p.Dist(ch.Hull.After(i))
	}
	return per
}

/*
	Jarvis match implementation (too slow to solve this problem):

	http://tomswitzer.net/2009/12/jarvis-march/

	func (ch ConvexHull) LeftmostPoint() Point {
		min := ch.Points[0]
		for _, p := range ch.Points {
			if p.x < min.x {
				min = p
			}
		}
		return min
	}

	func (ch *ConvexHull) FindHull() {
		var end Point
		hp := ch.LeftmostPoint()

		for len(ch.Hull) <= 0 || end != ch.Hull[0] {
			ch.Hull = append(ch.Hull, hp)
			end = ch.Points[0]

			for i, p := range ch.Points {
				if i > 0 && (end == hp || p.LeftOf(hp, end)) {
					end = p
				}
			}

			hp = end
		}
	}
*/

/*
	Different alignment algorithm that more closely approximates the slope of
	the edge, but is too slow to solve this problem:

	// Slope and inverse slope of the edge. Note that these are integers, so
	// these are rounded down.
	s := dy / dx
	is := dx / dy

	// The goal is to place a diagonal edge (from p to dp) and a side edge (from
	// dp to sp) that are both aligned to the grid. The slope of the edge from
	// p to sp will have slope s, the same as the edge from p to q.
	//
	// Depending on the steepness of the slope, the edge from dp to sp may be
	// vertical or horizontal.
	dp := Point{p.x + xdir, p.y + ydir}
	ch.Hull = ch.Hull.Insert(dp, i)

	var sp Point
	if s != 0 {
		sp.x = p.x + xdir
		sp.y = p.y + s*xdir
	} else {
		sp.x = p.x + is*ydir
		sp.y = p.y + ydir
	}

	// sp could equal q, so only insert if necessary.
	if sp.x != q.x && sp.y != q.y {
		ch.Hull = ch.Hull.Insert(sp, i+1)
	}

	// Skip over dp, since the next edge is between dp and sp and we know that
	// is aligned.
	i++
*/
