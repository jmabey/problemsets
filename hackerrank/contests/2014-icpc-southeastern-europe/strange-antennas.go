// HackerRank: Strange Antennas
// Go
// Author: Jimmy Mabey
//
// Note the distiction between lines, columns and x, y values. Antennas are
// positioned on lines and columns between cells and the cells themselves are
// positioned on x, y values.
//
// I'm not sure what type of algorithm this is, but I figured this out from
// scratch. There is a grid, non-arbitrary polygons are placed on a grid, and
// the number of covered cells must be counted. There is a condition for
// determining coverage: must be covered an odd number of polygons.
//
// The trick to this solution is to avoid looping over every cell in the grid.
// The grid can be large, the polygons themselves can be large, but there are
// only a small number of polygons. It is way more efficient to loop over every
// row and consider groups of concecutive cells that are covered. Since the
// polygons are well-defined, it is possible to determine how they cover cells
// in each row.

package main

import (
	"errors"
	"fmt"
	"sort"
)

// Possible antenna orientations represented as integers.
const (
	SW = iota
	SE
	NE
	NW
)

func main() {
	var n, a int
	fmt.Scanln(&n)
	fmt.Scanln(&a)

	g := NewGrid(n)
	for i := 0; i < a; i++ {
		var l, c, p, o int
		fmt.Scanln(&l, &c, &p, &o)
		g.Add(l, c, p, o)
	}

	fmt.Println(g.Coverage())
}

// Antenna represents an antenna positioned on the corners of the grid, with
// some power (how far the signal reaches), and its orientation.
type Antenna struct {
	Line, Column, Power, Orient int
}

// RowCoverage returns the cells that are covered by this antenna on row y or
// returns an error if there is no coverage. The two integers are starting and
// ending column numbers and the covered cells are in between these columns.
func (a Antenna) RowCoverage(y int) (int, int, error) {
	// How far away y is from the antenna.
	var dy int
	if a.Orient == SE || a.Orient == SW {
		dy = y - a.Line
	} else {
		dy = a.Line - y - 1
	}

	// How many cells on row y have coverage. Dependent on dy, so the further
	// away y is, the less coverage on this row.
	c := a.Power - dy
	if c > a.Power || c < 0 {
		return 0, 0, errors.New("antenna does not cover row")
	}

	if a.Orient == NE || a.Orient == SE {
		return a.Column, a.Column + c, nil
	}
	return a.Column - c, a.Column, nil
}

// Grid represents a square grid in which antennas are placed. Cells may or may
// not be covered by a signal from these antennas.
type Grid struct {
	Size     int
	Antennas []Antenna
}

// NewGrid initializes a grid of size n.
func NewGrid(n int) *Grid {
	return &Grid{Size: n}
}

// Add adds an antenna to the grid.
func (g *Grid) Add(l, c, p, o int) {
	g.Antennas = append(g.Antennas, Antenna{l, c, p, o})
}

// Coverage returns the number of cells covered by a signal, taking into account
// the cancellation of singnals if a cell is covered by an even number of
// signals.
func (g Grid) Coverage() int {
	cov := 0

	for y := 0; y < g.Size; y++ {
		// Consider all of the cells covered by each antenna in this row. One
		// antenna may cover a consecutive number of cells in this row. If an
		// antenna does cover some cells, keep the starting and ending number of
		// the columns containing those cells.
		var cs []int
		for _, a := range g.Antennas {
			if s, e, err := a.RowCoverage(y); err == nil {
				// Bound the column numbers to the grid so that cells outside of
				// the grid are not counted.
				if s > g.Size || e < 0 {
					continue
				}
				if s < 0 {
					s = 0
				}
				if e > g.Size {
					e = g.Size
				}

				cs = append(cs, s, e)
			}
		}

		// By sorting all the column numbers, each pair of numbers represent
		// a group of cells covered by the same number of antennas. Each number
		// represents the beginning or end of coverage for some antenna, thus
		// changing whether or not the signal cancels out.
		//
		// E.g., cs[0] to cs[1] are cells with a signal, cs[1] to cs[2] do not
		// have a signal, cs[2] to cs[3] do, cs[3] to cs[4] do not, etc. Every
		// other pair represents a group of cells having signals.
		sort.Ints(cs)

		// Thus every other pair is a group of cells with a signal in this row
		// and they are easily countable.
		for i := 0; i < len(cs); i += 2 {
			cov += cs[i+1] - cs[i]
		}

		// Since there are a small amount of antennas, there are much less
		// groups of cells than there are individual cells. Thus iterating over
		// these groups is significantly faster than iterating over every cell.
	}

	return cov
}

/*
	Previous attempts:

	1. Use a map to store the state of each point in the grid. Loop through all
	   the antennas, loop through all the points covered by each antenna and
	   toggle the state of each point. Count all the points that are true. Slow,
	   too much memory.

	2. Don't check the entire grid at once. Partition the grid into smaller
	   grids and check each smaller grid. Carefully bound things so it only
	   loops over points in the smaller grid. Fixes the memory problem, but
	   doesn't improve speed.

	3. Loop through all the points in the grid, loop through all the antennas
	   and count how many cover each point. Still doesn't improve speed since it
	   still touches every point.
*/

/*
	Incomplete attempt to partition the grid:

	type Point struct {
		X, Y int
	}

	func (g *Grid) Emit() int {
		// The maximum possible size of the grid is way too large to fit in
		// memory, so the grid needs to be partitioned.
		c := 0
		for x := 0; x < g.Size; x += MaxPartitionSize {
			for y := 0; y < g.Size; y += MaxPartitionSize {
				c += g.emitPartition(x, y)
				log.Println("COVERAGE", c)
			}
		}
		return c
	}

	func (g Grid) emitPartition(xmin, ymin int) int {
		cs := make(map[Point]bool)
		cov := 0

		// The largest x and y values that this partition contains.
		xmax, ymax := xmin+MaxPartitionSize-1, ymin+MaxPartitionSize-1
		if xmax >= g.Size {
			xmax = g.Size - 1
		}
		if ymax >= g.Size {
			ymax = g.Size - 1
		}

		log.Println("PARTITION", xmin, ymin, xmax, ymax)

		for a, on := range g.Antennas {
			if !on {
				continue
			}

			log.Println("ANTENNA", a)

			// Antennas are placed on the grid lines. Figure out which origin
			// cell the right angle of the signal triangle will be located and
			// the directions where the signal is emitting. Positive x is east,
			// positive y is south.
			xorig, yorig, xdir, ydir := a.Column, a.Line, 1, 1
			if a.Orient == NW || a.Orient == SW {
				xorig--
				xdir = -1
			}
			if a.Orient == NW || a.Orient == NE {
				yorig--
				ydir = -1
			}

			// Ignore this antenna if its coverage isn't in this partition at
			// all.
			yend := yorig + ydir*(a.Power-1)
			//xend := xorig + xdir*(a.Power-1)
			// TODO: Collision between square and triangle

			// Flip the signal state for all the cells in the signal's triangle
			// that are strictly within the current partition. Loop through each
			// covered row and toggle all the covered cells in the row.
			bystart, byend := Bound(yorig, ymin, ymax), Bound(yend, ymin, ymax)
			bxstart := Bound(xorig, xmin, ymax)

			for y := bystart; y != byend+ydir; y += ydir {
				// Where the signal ends on this row depends on how far away we are
				// from yorig.
				dy := ydir * (y - yorig)
				bxend := Bound(xorig+xdir*(a.Power-dy-1), xmin, xmax)

				for x := bxstart; x != bxend+xdir; x += xdir {
					sp := Point{x, y}
					cs[sp] = !cs[sp]
					log.Println(sp, cs[sp])

					if cs[sp] {
						cov++
					} else {
						cov--
					}
				}
			}
		}

		return cov
	}

	// Bound returns v bounded to the interval [min, max].
	func Bound(v, min, max int) int {
		if v < min {
			return min
		}
		if v > max {
			return max
		}
		return v
	}
*/
