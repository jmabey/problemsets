// HackerRank: Triples
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var m, n int
	fmt.Scanln(&m)
	fmt.Scanln(&n)

	fmt.Println(Sum(m, n))
}

// Sum returns the value of S_m,n (defined in the problem).
func Sum(m, n int) int {
	c := 0
	for j := 2; j <= n; j++ {
		c += Card(m, j)
	}
	return c
}

// Card returns the number of elements in T_m,j (defined in the problem).
func Card(m, j int) int {
	// Fermat's Last Theorem: no positive integers x, y, z satisfy
	// x^j + y^j = z^j for j > 2.
	//
	// Since x, y, z can be 0, then the only elements of T_m,j would be:
	// (0, a, a) where 0 <= a <= m
	if j > 2 {
		return m + 1
	}

	// Otherwise for j = 2, need to brute-force all the elements of T_m,j.
	c := 0
	for z := 0; z <= m; z++ {
		for y := 0; y <= z; y++ {
			for x := 0; x <= y; x++ {
				if x*x+y*y == z*z {
					c++
				}
			}
		}
	}
	return c
}
