// HackerRank: Banks
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	bs := make(Banks, n)
	for i := 0; i < len(bs); i++ {
		fmt.Scan(&bs[i])
	}

	fmt.Println(bs.MoveAll())
}

type Banks []int

// AnyNegative returns true if any bank has negative capital.
func (bs Banks) AnyNegative() bool {
	for i := 0; i < len(bs); i++ {
		if bs[i] < 0 {
			return true
		}
	}
	return false
}

// Smallest returns the index of the bank with the smallest capital.
func (bs Banks) Smallest() int {
	i := 0
	min := bs[0]
	for j := 1; j < len(bs); j++ {
		if bs[j] < min {
			i = j
			min = bs[j]
		}
	}
	return i
}

// MoveAll makes magic moves until all the banks have non-negative capitals and
// returns the number of moves.
func (bs Banks) MoveAll() int {
	m := 0
	for bs.AnyNegative() {
		i := bs.Smallest()

		// Neighbors of the first and last bank wrap around.
		nl := (i - 1 + len(bs)) % len(bs)
		nr := (i + 1) % len(bs)

		bs[nl] += bs[i]
		bs[nr] += bs[i]
		bs[i] *= -1
		m++
	}
	return m
}
