// HackerRank: Frame
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var x, y int
	fmt.Scanln(&x, &y)

	var n int
	fmt.Scanln(&n)

	for i := 0; i < n; i++ {
		var a int
		fmt.Scanln(&a)

		if Pave(x, y, a) {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}

// Pave returns true if a frame of size x by y can be paved with tiles of size
// a by 1.
func Pave(x, y, a int) bool {
	if a <= 0 || a > x || a > y {
		return false
	}

	// Each line corresponds to the following tiling strategies:
	//
	//    1. 111222333  2. 81112223  3. 411111  4. 112233  5. 91122
	//       8       4     8      3     4    2     9    4     9   3
	//       8       4     8      3     4    2     9    4     8   3
	//       8       4     7      4     4    2     8    5     8   4
	//       777666555     7      4     4    2     877665     7   4
	//                     76665554     333332                76655
	return (x%a == 0 && (y-2)%a == 0) ||
		(y%a == 0 && (x-2)%a == 0) ||
		((x-1)%a == 0 && (y-1)%a == 0) ||
		(x%a == 0 && (y-1)%a == 0 && (x-2)%a == 0) ||
		(y%a == 0 && (x-1)%a == 0 && (y-2)%a == 0)
}
