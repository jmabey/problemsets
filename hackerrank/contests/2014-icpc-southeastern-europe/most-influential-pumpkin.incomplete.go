// HackerRank: Most Influential Pumpkin
// Go
// Author: Jimmy Mabey
//
// TODO: One test case takes too long. I'm guessing it's the tracking of values
// greater than the median -- maintaining a large slice (120,000 elements). Try
// binary search trees?
//
// This solution uses a technique called "square root decomposition" (not sure
// if there is a better name, there aren't many resources on it). When a range
// of pumpkins is watered, as many whole blocks are watered as possible instead
// of individual pumpkins, significantly reducing the number of updates.
//
// The first time the median needs to be calculated involves sorting the slice
// of pumpkins and finding the median manually. After the first time, we can
// assume the median will at most change by 1 (since pumpkins only grow by
// 1 per watering).
//
// If we know the median is M, then less than half the numbers in a slice must
// be greater than M. If more than half are greater than M, then we know M is
// wrong. Since we know the median only increases by 1, then we can assume if
// M is wrong, then the new median is M + 1.
//
// In order to efficiently count the number of pumpkins larger than the known
// median, each block counts the number of values around the known median and
// keeps a running count of how many values are greater than the median.

package main

import (
	"fmt"
	"sort"
	"time"
)

const MaxWaterings = 60000

func main() {
	for {
		var n, k int
		fmt.Scanln(&n, &k)

		if n == 0 && k == 0 {
			break
		}

		bs := NewBlockSlice(n)
		for i := 0; i < n; i++ {
			var s int
			fmt.Scan(&s)
			bs.Set(i, s)
		}

		tt := time.Now()
		for i := 0; i < k; i++ {
			var s, t int
			fmt.Scanln(&s, &t)
			bs.Grow(s-1, t-1)

			fmt.Println(bs.Median())
		}
		fmt.Println(time.Since(tt))
	}
}

// BlockSlice implements a slice of n numbers grouped into blocks of size
// sqrt(n).
type BlockSlice struct {
	Size        int
	Length      int
	Blocks      []Block
	KnownMedian int
}

// NewBlockSlice initializes a new BlockSlice with size n.
func NewBlockSlice(n int) BlockSlice {
	bs := BlockSlice{
		Size:   10, //int(math.Ceil(math.Sqrt(float64(n)))),
		Length: n,
	}

	// Note the last block accounts for the remainder of elements and may not be
	// completely filled.
	bsn := n / bs.Size
	if n%bs.Size > 0 {
		bsn++
	}
	bs.Blocks = make([]Block, bsn)

	for i := 0; i < bsn; i++ {
		bn := bs.Size
		if i*bs.Size+bn > n {
			bn = n % bs.Size
		}

		bs.Blocks[i] = NewBlock(bn)
	}

	return bs
}

// Set sets element i to value v.
func (bs *BlockSlice) Set(i, v int) {
	bs.Blocks[i/bs.Size].Set(i%bs.Size, v)
}

// Grow increments the value of elements l to r (inclusive).
func (bs *BlockSlice) Grow(l, r int) {
	i := l
	for ; i%bs.Size > 0 && i <= r; i++ {
		bs.Blocks[i/bs.Size].Grow(i % bs.Size)
	}
	for ; i+bs.Size-1 <= r; i += bs.Size {
		bs.Blocks[i/bs.Size].GrowAll()
	}
	for ; i <= r; i++ {
		bs.Blocks[i/bs.Size].Grow(i % bs.Size)
	}
}

// Median returns the median of the entire slice.
func (bs *BlockSlice) Median() int {
	// First call requires calculating the median manually. After the first
	// call, we can count the number of values greater than the median and if
	// this is more than half the slice, then the median is incremented. This is
	// under the assumption that the median only increases by at most 1.
	if bs.KnownMedian <= 0 {
		ss := []int{}
		for _, b := range bs.Blocks {
			for _, s := range b.Values {
				ss = append(ss, s+b.Growth)
			}
		}
		sort.Ints(ss)
		bs.KnownMedian = ss[len(ss)/2]

		for i := range bs.Blocks {
			bs.Blocks[i].CountFrom(bs.KnownMedian)
		}
	} else {
		t := 0
		inc := false
		for _, b := range bs.Blocks {
			t += b.Greater
			if t > bs.Length/2 {
				inc = true
				break
			}
		}
		if inc {
			bs.KnownMedian++
			for i := range bs.Blocks {
				bs.Blocks[i].CountNext()
			}
		}
	}

	return bs.KnownMedian
}

// Block is a group of values in a BlockSlice.
type Block struct {
	Growth int
	Values []int
	Sorted []int

	Greater    int
	CountMin   int
	CountIndex int
	Counts     []int
}

// NewBlock initializes a new block with size n.
func NewBlock(n int) Block {
	return Block{Values: make([]int, n), Sorted: make([]int, n)}
}

// Set sets element i to value v.
func (b *Block) Set(i, v int) {
	b.Values[i] = v
}

// GrowAll increments all values of all elements in this block.
func (b *Block) GrowAll() {
	// To avoid a loop when updating value counts, shift the count array so that
	// any given element now stores the count one greater than the value it was
	// originally counting.
	if b.Counts != nil {
		b.Greater += b.Counts[b.CountIndex]
		b.CountMin++
		b.CountIndex--
	}

	b.Growth++
}

// Grow increments the value of element i.
func (b *Block) Grow(i int) {
	if b.Values[i]+b.Growth == b.CountMin+b.CountIndex {
		b.Greater++
	}
	b.countAdd(b.Values[i], -1)
	b.Values[i]++
	b.countAdd(b.Values[i], 1)
}

// countAdd increments the count for value v by n.
func (b *Block) countAdd(v, n int) {
	if i := v + b.Growth - b.CountMin; i >= 0 && i < len(b.Counts) {
		b.Counts[i] += n
	}
}

// CountFrom begins the tracking of values in the range [m-n, n+m]. Whenever
// Grow or GrowAll is called, values within this range are counted and the
// Greater field stores how many values are greater than m.
func (b *Block) CountFrom(m int) {
	b.CountMin = m - MaxWaterings
	b.CountIndex = MaxWaterings
	b.Counts = make([]int, 2*MaxWaterings+1)

	for _, v := range b.Values {
		if v+b.Growth > m {
			b.Greater++
		}
		b.countAdd(v, 1)
	}
}

// CountNext adjusts the Greater field as follows: if the Greater field stores
// the number of values greater than m, it will now store the number of values
// greater than m + 1.
func (b *Block) CountNext() {
	b.CountIndex++
	if b.CountIndex < len(b.Counts) {
		b.Greater -= b.Counts[b.CountIndex]
	}
}

/*
	Attempts:

	1. Sort the slice of pumpkins and output the median after every line. Too
	   slow.

	2. Keeping a running count of each pumpkin size. Slower than attempt #1.

	3. Keep a sorted slice of pumpkins, but swap elements to maintain sorting
	   instead of calling a sort.Ints every time.

	4. The only time the median can change is when a pumpkin grows larger than
	   it. Cache the median and only recalculate it when this happens. Faster,
	   but still too slow.

	5. The number of pumpkins larger than the median can be easily tracked. If
	   this number is larger than half of the number pumpkins, then the median
	   increased by 1. Then recount the number of pumpkins larger than the new
	   median. The looping required for recounting is too slow.

	6. Eliminate the looping by tracking a count for each pumpkin size and
	   subtracting it from the number of larger pumpkins. Turns out to be the
	   same speed as attempt #3.

	7. map[int]int is very slow. The median can only increment at most by the
	   number of waterings (k), so it's reasonable to track only the counts
	   starting from the initial median m to m+k inclusive. 10x faster.

	8. Did research and discovered a technique called "square root
	   decomposition", basically grouping a slice into sqrt(n) blocks so that
	   operations on a range happen on blocks. Since there are much less blocks
	   than elements, these operations happen much faster.

	9. Replaced sort.Ints with manual sorting of values. Calculating the median
	   went from ms to us range.

	10. Use binary search for Block.Greater and Block.sort.

	11. Implement counting pumpkin sizes again, to avoid any kind of loops.
		Additional state needs to be tracked to handle growing an entire block.
*/

/*
	Using a map to store sizes:

	Sizes map[int]int

	func (g *Garden) Median() int {
		// Collect all the counts of different sizes of pumpkins into a sorted
		// slice, then the median is the size when we sum all the counts and the
		// sum is at least half of the pumpkins.
		var ss []int
		for s, c := range g.Sizes {
			if c > 0 {
				ss = append(ss, s)
			}
		}
		sort.Ints(ss)

		t := 0
		for _, s := range ss {
			t += g.Sizes[s]
			if t >= len(g.Pumpkins)/2 {
				return s
			}
		}

		return 0
	}
*/

/*
	Partial sort after pumpkin grows:

	Sorted []int

	// Maintain a sorted slice of pumpkin sizes to quickly find the median.
	// Instead of performing a full sort each time, increment and swap elements
	// until the slice is sorted.
	if g.Sorted == nil {
		g.Sorted = make([]int, len(g.Pumpkins))
		copy(g.Sorted, g.Pumpkins)
		sort.Ints(g.Sorted)
	} else {
		si := 0
		for j, s := range g.Sorted {
			if s == g.Pumpkins[i]-1 {
				si = j
				break
			}
		}

		g.Sorted[si]++
		for j := si; j < len(g.Sorted)-1 && g.Sorted[j+1] < g.Sorted[j]; j++ {
			g.Sorted[j], g.Sorted[j+1] = g.Sorted[j+1], g.Sorted[j]
		}
	}
*/

/*
	Careful tracking of median and pumpkin sizes that affect the median:

	// Garden tracks the size of the pumpkins in the garden as well as
	// statistics required for updating the median efficiently: the known value
	// for the median, how many pumpkins are larger than the known median, and
	// running counts for every pumpkin size (from the median value to the max
	// possible median value after k growths).
	type Garden struct {
		Pumpkins   []int
		Waterings  int
		Med        int
		Larger     int
		SizeMin    int
		SizeCounts []int
	}

	// NewGarden initializes a garden with n pumpkins.
	func NewGarden(n, k int) *Garden {
		return &Garden{
			Pumpkins:   make([]int, n+1),
			SizeCounts: make([]int, k+2),
			Waterings:  k,
		}
	}

	// Set sets the size of pumpkin i to s.
	func (g *Garden) Set(i, s int) {
		g.Pumpkins[i] = s
	}

	// Grow grows the size of pumpkin i by 1 and updates internal statistics.
	func (g *Garden) Grow(i int) {
		g.initMedian()

		s := g.Pumpkins[i]
		if s < g.Med+1-g.Waterings || s >= g.SizeMin+len(g.SizeCounts) {
			return
		}

		if s == g.Med {
			g.Larger++
		}
		g.addSize(s+1, 1)
		g.addSize(s, -1)

		g.Pumpkins[i]++
	}

	// initMedian initializes the median value for the first time,
	func (g *Garden) initMedian() {
		if g.Med != 0 {
			return
		}

		// Median needs to be calculated by sorting the pumpkins.
		s := make([]int, len(g.Pumpkins))
		copy(s, g.Pumpkins)
		sort.Ints(s)

		g.Med = s[len(s)/2]

		// Count how many pumpkins are larger than the median. This data is
		// required for updating the median efficiently.
		g.Larger = 0
		g.SizeMin = g.Med
		for _, s := range g.Pumpkins {
			if s > g.Med {
				g.Larger++
				g.addSize(s, 1)
			}
		}
	}

	// addSize adds v to the count for size s, if size s is being counted.
	func (g *Garden) addSize(s, v int) {
		if s < g.SizeMin || s >= g.SizeMin+len(g.SizeCounts) {
			return
		}
		g.SizeCounts[s-g.SizeMin] += v
	}

	// Median returns the median size of all pumpkins in the garden.
	func (g *Garden) Median() int {
		if g.Larger >= len(g.Pumpkins)/2 {
			// Once calculated, the median increases every time the number of
			// pumpkins greater than the currently known median is more than
			// half the number of pumpkins. Since we're tracking a count for
			// each size, we can easily determine how many pumpkins are larger
			// than the new median.
			g.Med++
			g.Larger -= g.SizeCounts[g.Med-g.SizeMin]
		}

		g.Waterings--
		return g.Med
	}
*/

/*
	Binary search and maintaining sorting of block values:

	t := 0
	for _, b := range bs.Blocks {
		t += b.Greater(bs.KnownMedian)
		if t > bs.Length/2 {
			bs.KnownMedian++
			break
		}
	}

	// sort maintains the sorted slice of element values.
	func (b *Block) sort(sOld, sNew int) {
		i := sort.Search(len(b.Sorted), func(i int) bool {
			return b.Sorted[i] > sOld
		})

		// Searched for the lowest indexed element > sOld (or len(b.Sorted) if
		// not found), so i - 1 is the index of sOld.
		i--

		b.Sorted[i] = sNew
		for ; i < len(b.Sorted)-1 && b.Sorted[i+1] < b.Sorted[i]; i++ {
			b.Sorted[i], b.Sorted[i+1] = b.Sorted[i+1], b.Sorted[i]
		}
	}

	// Greater returns the number of elements whose values are greater than v.
	func (b *Block) Greater(v int) int {
		i := sort.Search(len(b.Sorted), func(i int) bool {
			return b.Sorted[i]+b.Growth > v
		})
		if i >= len(b.Sorted) {
			return 0
		}
		return len(b.Sorted) - i
	}
*/
