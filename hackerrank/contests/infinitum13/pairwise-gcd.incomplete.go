// HackerRank: Pairwise GCD
// Go
// Author: Jimmy Mabey
//
// Properties of modulus:
// https://en.wikipedia.org/wiki/Modulo_operation#Equivalencies
//
// The key property: gcd(a + mb, b) = gcd(a, b)
// In other words: gcd(a%b, b) = gcd(a, b)
//
// A count of each possible modulus values for a given element can be tracked
// and used to avoid recalculating GCDs.
//
// TODO: With n=10^5, looping through all pairs *doing nothing* takes longer
// than the time limit.
//
// https://en.wikipedia.org/wiki/Euler%27s_totient_function
// This might be useful, as it relates to GCD. phi(n) is the number of integers
// from 1 to n having a GCD of 1 with n. It looks like most GCDs are 1.

package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

// Mod is used to output the sum modulo this value.
const Mod = 1234567891

// MaxEl is the maximum value of an element in the sequence.
const MaxEl = 100000

func main() {
	in := bufio.NewReader(os.Stdin)

	var n, m int
	fmt.Fscanln(in, &n, &m)

	a := make([]int, n)
	for i := range a {
		fmt.Fscan(in, &a[i])
	}
	sort.Ints(a)

	b := make([]int, m+1)
	for i := range b {
		fmt.Fscan(in, &b[i])
	}

	var (
		modCounts []int
		gcdCounts = make([]int, MaxEl+1)
	)
	for i := range a {
		if len(modCounts) != a[i] {
			modCounts = make([]int, a[i])
			for j := i + 1; j < len(a); j++ {
				modCounts[a[j]%a[i]]++
			}
		} else {
			modCounts[0]--
		}
		for j, c := range modCounts {
			if c > 0 {
				gcdCounts[GCD(a[i], j)] += c
			}
		}
	}

	sum := 0
	for i, c := range gcdCounts {
		if c > 0 {
			sum = (sum + c*PMod(b, i)) % Mod
		}
	}
	if sum < 0 {
		sum += Mod
	}

	fmt.Println(sum)
}

// GCD returns the greatest common divisor of a and b.
func GCD(a, b int) int {
	if a < 0 {
		a *= -1
	}
	if b < 0 {
		b *= -1
	}
	if b > a {
		a, b = b, a
	}

	for b != 0 {
		a, b = b, a%b
	}

	return a
}

// PMod returns the value of a polynomial with coefficients cs at x, modulo Mod.
func PMod(cs []int, x int) int {
	sum := 0
	for i, c := range cs {
		v := c % Mod
		for j := 0; j < i; j++ {
			v = (v * (x % Mod)) % Mod
		}
		sum = (sum + v) % Mod
	}
	return sum
}

/*
	Previous attempts:

	1. Used ints with properties of modulus to avoid using big.Ints. Wrong
	   answer.

	2. Switched to big.Int with modulus for the output. Correct, but somewhat slow.

	3. Figured out modulus with ints could output a negative number. Add Mod to
	   ensure the output is positive. Correct and faster, but still times out.

	4. Added caching of PMod output. Faster, still times out.

	5. Remove caching in favour of counting GCDs and calling PMod once for each
	   count. Faster, still times out. Calculating GCDs looks like O(n^2), which
	   is the major bottleneck.

	6. gcd(a%b, b) = gcd(a, b). Can count possible modulus values and reduce the
	   number of GCD calculations.
*/
