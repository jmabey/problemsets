// HackerRank: Integral Points
// Go
// Author: Jimmy Mabey
//
// Pick's theorem:
// https://en.wikipedia.org/wiki/Pick%27s_theorem
//
// Area of a triangle given three points:
// http://www.mathopenref.com/coordtrianglearea.html
//
// Counting integral points along a line:
// http://math.stackexchange.com/questions/628117/how-to-count-lattice-points-on-a-line
//
// GCD (Euclid's algorithm):
// https://en.wikipedia.org/wiki/Greatest_common_divisor#Using_Euclid.27s_algorithm
//
// Pick's theorem is rearranged to solve for the number of lattice points, since
// we can calculate the area and number of boundary points.

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var t Triangle
		fmt.Scanln(
			&t.Points[0].X, &t.Points[0].Y,
			&t.Points[1].X, &t.Points[1].Y,
			&t.Points[2].X, &t.Points[2].Y,
		)

		// Doubling avoids using floats, which introduce precision problems.
		fmt.Println((t.DoubleArea() - t.BoundaryPoints() + 2) / 2)
	}
}

// Point represents a point in 2D space.
type Point struct {
	X, Y int
}

// Triangle represents a triangle in 2D space.
type Triangle struct {
	Points [3]Point
}

// DoubleArea returns twice the area of the triangle, avoiding floating point
// precision issues.
func (t Triangle) DoubleArea() int {
	a := t.Points[0].X*(t.Points[1].Y-t.Points[2].Y) +
		t.Points[1].X*(t.Points[2].Y-t.Points[0].Y) +
		t.Points[2].X*(t.Points[0].Y-t.Points[1].Y)
	if a < 0 {
		a *= -1
	}
	return a
}

// BoundaryPoints returns the number of integral points (or lattice points)
// along the boundary of the triangle.
func (t Triangle) BoundaryPoints() int {
	bp := 0

	// Each line is one less than the number of actual boundary points, because
	// each point of the triangle is part of two lines, so don't count it twice.
	for i := 0; i < len(t.Points); i++ {
		j := (i + 1) % len(t.Points)
		bp += GCD(t.Points[j].X-t.Points[i].X, t.Points[j].Y-t.Points[i].Y)
	}

	return bp
}

// GCD returns the greatest common divisor of a and b.
func GCD(a, b int) int {
	if a < 0 {
		a *= -1
	}
	if b < 0 {
		b *= -1
	}
	for b != 0 {
		a, b = b, a%b
	}
	return a
}
