// HackerRank: Game with Coins
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var a, b int
		fmt.Scanln(&a, &b)

		// A side has to be empty or one side has to be even and the other odd
		// for First to win.
		if a == 0 || b == 0 || a%2 != b%2 {
			fmt.Println("First")
		} else {
			fmt.Println("Second")
		}
	}
}
