// HackerRank: Multiply!
// Go
// Author: Jimmy Mabey
//
// Sieve for prime factorization:
// http://www.drmaciver.com/2012/08/sieving-out-prime-factorizations/
//
// TODO: Full sieve vs lowest prime factor sieve: full is faster in a few cases,
// but doesn't help the test cases with time exceeded.

package main

import (
	"bufio"
	"fmt"
	"os"
)

// Mod is applied as a modulus for the output.
const Mod = 1000000007

// MaxX is the maximum allowed value of x from the input.
const MaxX = 1000000

func main() {
	in := bufio.NewReader(os.Stdin)
	s := NewSolver()

	var n, q int
	fmt.Fscanln(in, &n, &q)
	s.Count(n)

	for i := 0; i < q; i++ {
		var x int
		fmt.Fscanln(in, &x)

		s.Count(x)
		fmt.Println(s.SumMod())
	}
}

// Solver contains a sieve of prime factors for all allowed numbers and keeps
// a running total for terms needed to calculate sigma_1(n) (the sum of divisors
// function).
type Solver struct {
	Sieve  [][]int
	Powers map[int]int
	Sums   map[int]int
}

// NewSolver initializes a solver instance.
func NewSolver() *Solver {
	s := make([][]int, MaxX+1)
	for i := 2; i < len(s); i++ {
		if s[i] == nil {
			for j := i; j < len(s); j *= i {
				for k := j; k < len(s); k += j {
					s[k] = append(s[k], i)
				}
			}
		}
	}

	return &Solver{
		Sieve:  s,
		Powers: map[int]int{},
		Sums:   map[int]int{},
	}
}

// Count determines the prime factors of n and adds them to the running count.
func (s *Solver) Count(n int) {
	for _, p := range s.Sieve[n] {
		if s.Powers[p] == 0 {
			s.Powers[p] = 1
			s.Sums[p] = 1
		}
		s.Powers[p] = (s.Powers[p] * p) % Mod
		s.Sums[p] = (s.Sums[p] + s.Powers[p]) % Mod
	}
}

// SumMod returns the sum of all prime factors, modulo Mod.
func (s *Solver) SumMod() int {
	prod := 1
	for _, sum := range s.Sums {
		if sum <= 0 {
			continue
		}

		prod = (prod * sum) % Mod
	}
	return prod
}

/*
	Finding prime factors of a number using a list of primes:

	for i := 0; s.Primes[i] <= n; i++ {
		p := s.Primes[i]
		if n%p == 0 {
			if s.Powers[p] == 0 {
				s.Powers[p] = 1
				s.Sums[p] = 1
			}
			s.Powers[p] = (s.Powers[p] * p) % Mod
			s.Sums[p] = (s.Sums[p] + s.Powers[p]) % Mod

			n /= p
			i--
		}
	}
*/

/*
	Using a sieve storing the lowest prime factor for each number:

	// s[n] = the lowest prime factor of n
	s := make([]int, MaxX+1)
	for i := 2; i < len(s); i++ {
		if s[i] == 0 {
			s[i] = i
			for j := i; i*j < len(s); j++ {
				if s[i*j] == 0 {
					s[i*j] = i
				}
			}
		}
	}

	for n > 1 {
		p := s.Sieve[n]

		if s.Powers[p] == 0 {
			s.Powers[p] = 1
			s.Sums[p] = 1
		}
		s.Powers[p] = (s.Powers[p] * p) % Mod
		s.Sums[p] = (s.Sums[p] + s.Powers[p]) % Mod

		n /= p
	}
*/
