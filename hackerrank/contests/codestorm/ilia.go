// HackerRank: Counting Triangles
// Go
// Author: Jimmy Mabey

package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var n int
	fmt.Fscanln(in, &n)

	s := make([]int, n)
	for i := range s {
		fmt.Fscan(in, &s[i])
	}

	a, r, o := 0, 0, 0
	for i := 0; i < n-2; i++ {
		for j := i + 1; j < n-1; j++ {
			// Comparing a^2 + b^2 to c^2 tells you the type of triangle. Given
			// a list of sticks, figure out where the acute and obtuse triangles
			// begin and end, then count the sticks between these positions.
			// Using binary search is O(log n).
			absq := s[i]*s[i] + s[j]*s[j]

			// Index of the largest possible stick creating a valid triangle.
			max := sort.Search(n, func(k int) bool {
				return s[k] >= s[i]+s[j]
			}) - 1
			if max <= j {
				continue
			}

			// Index of the last acute triangle.
			aEnd := sort.Search(n, func(k int) bool {
				return absq <= s[k]*s[k]
			}) - 1

			// Index of the first obtuse triangle.
			oStart := sort.Search(n, func(k int) bool {
				return absq < s[k]*s[k]
			})

			o += max - oStart + 1
			r += oStart - aEnd - 1
			a += aEnd - j
		}
	}

	fmt.Println(a, r, o)
}

/*
	Brute-force search (O(n^3), too slow):

	a, r, o := 0, 0, 0
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			for k := j + 1; k < n; k++ {
				if s[i]+s[j] <= s[k] {
					continue
				}

				absq, csq := s[i]*s[i]+s[j]*s[j], s[k]*s[k]
				if absq > csq {
					a++
				} else if absq < csq {
					o++
				} else {
					r++
				}
			}
		}
	}
*/
