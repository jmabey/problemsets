// HackerRank: Game with a Boomerang
// Go
// Author: Jimmy Mabey

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var t int
	fmt.Fscanln(in, &t)

	for i := 0; i < t; i++ {
		var n int
		fmt.Fscanln(in, &n)

		// As far as I can tell, the pattern is this:
		//
		// Consider this sequence: f(x) = (4^x - 1)/3
		// https://oeis.org/A002450
		//
		// There exists some k such that f(k-1) < n <= f(k).
		// If n <= 2*f(k-1) + 1, then the result is n - f(k-1).
		// Otherwise, the result is n - floor((f(k) - n) / 2).
		low, high, pow := 0, 1, 1
		for n > high {
			low = high
			pow *= 4
			high = (pow - 1) / 3
		}
		mid := low * 2

		var out int
		if n <= mid {
			out = n - low
		} else {
			out = n - (high-n)/2
		}

		fmt.Println(out)
	}
}

/*
	Brute force (input size can be too large to fit in memory and too slow):

	a := make([]int, n)
	for j := 0; j < n; j++ {
		a[j] = j + 1
	}

	for j := 0; len(a) > 1; j = (j + 1) % len(a) {
		if len(a)%2 == 1 {
			// Hit themself: next person moves back a position.
			a = append(a[:j], a[j+1:]...)
			j--
		} else {
			// Hit someone across: if index is lower, then the next person
			// moves back a position.
			hit := (j + len(a)/2) % len(a)
			a = append(a[:hit], a[hit+1:]...)
			if hit < j {
				j--
			}
		}
	}
*/
