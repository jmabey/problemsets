// HackerRank: Save Quantumland
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n int
		fmt.Scanln(&n)

		hired, unguarded := 0, 0
		for j := 0; j < n; j++ {
			var b int
			fmt.Scan(&b)

			if b == 0 {
				unguarded++
			}

			// Every two unguarded cities in a row needs one guard.
			if b != 0 || j == n-1 {
				hired += unguarded / 2
				unguarded = 0
			}
		}

		fmt.Println(hired)
	}
}
