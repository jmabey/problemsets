// HackerRank: Emma's Notebook
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	// https://oeis.org/A024206, (n-1) * (n+3) / 4 and n = t + 1.
	fmt.Println(t * (t + 4) / 4)
}
