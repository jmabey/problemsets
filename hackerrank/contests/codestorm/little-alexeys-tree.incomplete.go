// HackerRank: Little Alexey's Tree
// Go
// Author: Jimmy Mabey
//
// TODO: There is probably a way to prune the search further. I tried comparing
// the running string to the maxStr (or a prefix of maxStr), but that didn't
// seem to help.

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var n int
	fmt.Fscanln(in, &n)

	t := NewTree(n)
	for i := 0; i < n-1; i++ {
		var (
			a, b int
			c    string
		)
		fmt.Fscanln(in, &a, &b, &c)

		t.Vertices[a].Edges = append(t.Vertices[a].Edges, Edge{To: b, Letter: c})
		t.Vertices[b].Edges = append(t.Vertices[b].Edges, Edge{To: a, Letter: c})
	}

	out := []int{}
	for i := 1; i < len(t.Vertices); i++ {
		out = append(out, t.Search(i))
	}

	fmt.Println(strings.Trim(fmt.Sprintf("%d", out), "[]"))
}

// Vertex represents a single vertex with its edges in the tree.
type Vertex struct {
	Edges []Edge
}

// Edge represents an edge to some destination vertex and the letter written on
// it.
type Edge struct {
	To     int
	Letter string
}

// Visit contains the state for a single visit of a vertex when searching the
// tree.
type Visit struct {
	From, Index int
	Visited     map[int]bool
	String      string
}

// Tree is a slice of all the vertices in the tree.
type Tree struct {
	Vertices []Vertex
	Results  map[int]Visit
}

// NewTree initializes a new tree with n vertices.
func NewTree(n int) *Tree {
	return &Tree{
		Vertices: make([]Vertex, n+1),
		Results:  map[int]Visit{},
	}
}

// Search returns the largest index for the vertex ending the lexicographically
// largest string beginning at vertex i.
func (t Tree) Search(i int) int {
	var (
		visits   = []Visit{{Index: i, Visited: map[int]bool{i: true}}}
		maxIndex int
		maxStr   string
	)
	for len(visits) > 0 {
		v := visits[0]
		visits = visits[1:]

		// Find the edges with the largest letter, excluding the current vertex.
		var (
			maxLetter string
			maxEdges  []Edge
		)
		for _, e := range t.Vertices[v.Index].Edges {
			if e.To == v.From {
				continue
			}
			if e.Letter > maxLetter {
				maxLetter = e.Letter
				maxEdges = []Edge{}
			}
			if e.Letter == maxLetter {
				maxEdges = append(maxEdges, e)
			}
		}

		// Reached a leaf vertex: compare completed string.
		if len(maxEdges) <= 0 {
			if v.String > maxStr || (v.String == maxStr && v.Index > maxIndex) {
				maxIndex, maxStr = v.Index, v.String
				t.Results[i] = v
			}
			continue
		}

		// Check what string is going to be created by visiting each edge (the
		// string will be the same for each edge) and if it's less than a prefix
		// of the largest known string of the same size, then we don't need to
		// visit it. Update the max string accordingly if we find a larger
		// string.
		/*
			TODO: Makes it slower.

			var prunedEdges []Edge
			for _, e := range maxEdges {
				s := fmt.Sprint(v.String, e.Letter)

				prefix := maxStr
				if len(s) < len(prefix) {
					prefix = prefix[:len(s)]
				}
				if s < prefix {
					continue
				}
				if s > maxStr || (s == maxStr && e.To > maxIndex) {
					maxIndex, maxStr = e.To, s
				}

				prunedEdges = append(prunedEdges, e)
			}
		*/

		for _, e := range prunedEdges {
			// TODO: Not sure if storing the results helps with pruning.
			next := Visit{
				From:    v.Index,
				Index:   e.To,
				Visited: map[int]bool{v.Index: true},
				String:  fmt.Sprint(v.String, e.Letter),
			}
			for j := range v.Visited {
				next.Visited[j] = true
			}

			// If a result already exists for the next vertex and doesn't go
			// through any vertices already visited, then it's already known
			// what the largest string is from this vertex onward, thus skipping
			// a lot of visits.
			if r, ok := t.Results[e.To]; ok && r.Index != i {
				for j := range r.Visited {
					if next.Visited[j] {
						ok = false
						break
					}
				}

				if ok {
					next.From = r.From
					next.Index = r.Index
					next.String = fmt.Sprint(v.String, e.Letter, r.String)
					for j := range r.Visited {
						next.Visited[j] = true
					}
				}
			}

			visits = append([]Visit{next}, visits...)
		}
	}

	return maxIndex
}

/*
	Recursive solution (too slow):

	func (vs Vertices) Search(i int) int {
		end, _ := vs.visit(0, i)
		return end
	}

	// visit returns the largest index for the vertex ending the
	// lexicographically largest string and the string itself from some vertex
	// to another vertex.  Returns "to" and an empty string if there are no more
	// edges to visit.
	func (vs Vertices) visit(from, to int) (int, string) {
		var (
			maxLetter string
			maxEdges  []Edge
		)
		for _, e := range vs[to].Edges {
			if e.To == from {
				continue
			}
			if e.Letter > maxLetter {
				maxLetter = e.Letter
				maxEdges = []Edge{}
			}
			if e.Letter == maxLetter {
				maxEdges = append(maxEdges, e)
			}
		}
		if len(maxEdges) <= 0 {
			return to, ""
		}

		// Recursively visit all edges and store the result for the
		// lexicographically largest string.
		maxI, maxStr := 0, ""
		for _, e := range maxEdges {
			i, s := vs.visit(to, e.To)
			s = fmt.Sprint(e.Letter, s)
			if s > maxStr || (s == maxStr && i > maxI) {
				maxI, maxStr = i, s
			}
		}
		return maxI, maxStr
	}
*/
