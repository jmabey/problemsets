// TODO: Should figure out the largest number of move operations n, brute force
// the cost from n to 0

package main

import "fmt"

func main() {
	var n, k, l int
	fmt.Scanln(&n, &k, &l)

	a := make([]int, n)
	sum := 0
	max := 0
	for i := 0; i < n; i++ {
		fmt.Scan(&a[i])
		sum += a[i]
		if a[i] > max {
			max = a[i]
		}
	}

	// Sum of all elements don't divide evenly into number of elements: need to
	// increment until this is the case.
	incOps := 0
	if r := sum % n; r != 0 {
		incOps = n - r
	}

	// All elements need to be equal to average of sum after incrementing.
	// Sum differences above and below average and the number of decrement and
	// increment operations is the smallest of the two.
	above, below := 0, 0
	avg := (sum + incOps) / n
	for i := 0; i < n; i++ {
		if a[i] > avg {
			above += a[i] - avg
		} else if a[i] < avg {
			below += avg - a[i]
		}
	}

	moveOps := above
	if below < above {
		moveOps = below
	}

	// Another strategy is increasing everything up to the largest element.
	allIncOps := 0
	for i := 0; i < n; i++ {
		allIncOps += max - a[i]
	}

	mixCost := k*moveOps + l*incOps
	incCost := l * allIncOps

	if mixCost > incCost {
		fmt.Println(incCost)
	} else {
		fmt.Println(mixCost)
	}
}
