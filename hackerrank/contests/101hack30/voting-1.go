// HackerRank: Voting
// Go
// Author: Jimmy Mabey

package main

import "fmt"

func main() {
	var m int
	fmt.Scanln(&m)

	var (
		vs   = map[string]int{}
		maxv int
		maxn string
	)
	for i := 0; i < m; i++ {
		var n string
		fmt.Scanln(&n)

		vs[n]++

		if vs[n] > maxv {
			maxv = vs[n]
			maxn = n
		}
	}

	fmt.Println(maxn)
}
