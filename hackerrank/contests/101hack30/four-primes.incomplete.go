// HackerRank: Four Primes
// Go
// Author: Jimmy Mabey
//
// TODO: Too slow

package main

import (
	"fmt"
	"math"
)

const MaxN = 8000

func main() {
	var t int
	fmt.Scanln(&t)

	ps := Primes()
	for i := 0; i < t; i++ {
		var n int
		fmt.Scanln(&n)

		if OrPrimes(ps, n) {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}

func Primes() []int {
	c := make([]bool, MaxN+1)
	for i := 2; i <= MaxN; i++ {
		if c[i] {
			continue
		}
		for j := i * 2; j <= MaxN; j += i {
			c[j] = true
		}
	}

	ps := []int{}
	for i := 2; i < len(c); i++ {
		if !c[i] {
			ps = append(ps, i)
		}
	}

	return ps
}

func OrPrimes(ps []int, n int) bool {
	// Reject even numbers > 2.
	if n > 2 && n%2 != 1 {
		return false
	}

	maxi := 0
	for i := 0; i < len(ps) && ps[i] <= n; i++ {
		maxi = i
	}

	// Check all possible combinations of primes smaller than n, starting with
	// the largest primes smaller than n. If the log2 of a prime is smaller than
	// the log2 of n, then there aren't enough bits in it (or any smaller
	// primes) to represent n. In other words the largest bit of n is 1, but the
	// largest bit of the prime and all smaller primes are 0.
	for i := maxi; i >= 0; i-- {
		if ps[i] == n {
			return true
		}
		if int(math.Log2(float64(ps[i]))) < l2n {
			return false
		}

		for j := i - 1; j >= 0; j-- {
			if ps[i]|ps[j] == n {
				return true
			}

			for k := j - 1; k >= 0; k-- {
				if ps[i]|ps[j]|ps[k] == n {
					return true
				}

				for l := k - 1; l >= 0; l-- {
					if ps[i]|ps[j]|ps[k]|ps[l] == n {
						return true
					}
				}
			}
		}
	}

	return false
}
