// HackerRank: Sergey and Homework
// Go
// Author: Jimmy Mabey
//
// TODO: Too slow

package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, k int
	fmt.Scanln(&n, &k)

	probs := make([]int, n)
	for i := range probs {
		fmt.Scan(&probs[i])
	}

	imp := make([]int, n)
	for i := range imp {
		fmt.Scan(&imp[i])
	}

	for k > 0 {
		var max, maxi int
		for i := range imp {
			if v := imp[i] * probs[i]; v > max {
				max, maxi = v, i
			}
		}

		// Solved all problems?
		if max <= 0 {
			break
		}

		// Figure out how importance * problems for the second largest subject.
		// This is what we need to get the largest subject below.
		var goal int
		for i := range imp {
			if v := imp[i] * probs[i]; i != maxi && v > goal {
				goal = v
			}
		}

		// How many problems should be left for the largest subject before we
		// move on to the next one.
		goalProbs := goal / imp[maxi]

		solved := probs[maxi] - goalProbs
		if solved > k {
			solved = k
		}

		k -= solved
		probs[maxi] -= solved
	}

	fmt.Println(strings.Trim(fmt.Sprintf("%d", probs), "[]"))
}
