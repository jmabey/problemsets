// HackerRank: 2D Array - DS
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"math"
)

// Size is the size of both dimensions of the array.
const Size = 6

func main() {
	var a [Size][Size]int
	for i := range a {
		for j := range a[i] {
			fmt.Scan(&a[i][j])
		}
	}

	max := math.MinInt64
	for i := 0; i < len(a)-2; i++ {
		for j := 0; j < len(a[i])-2; j++ {
			s := a[i+1][j+1]
			for k := 0; k < 3; k++ {
				s += a[i][j+k] + a[i+2][j+k]
			}

			if s > max {
				max = s
			}
		}
	}

	fmt.Println(max)
}
