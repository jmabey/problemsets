// HackerRank: Arrays- DS
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	fmt.Scanln(&n)

	a := make([]int, n)
	for i := n - 1; i >= 0; i-- {
		fmt.Scan(&a[i])
	}

	fmt.Println(strings.Trim(fmt.Sprintf("%d", a), "[]"))
}
