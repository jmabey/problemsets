// HackerRank: Insert a node into a sorted doubly linked list
// C++
// Author: Jimmy Mabey

Node* SortedInsert(Node *head, int data) {
	Node *n = new Node;
	n->data = data;

	if (head == NULL) {
		return n;
	}
	if (head->data >= data) {
		n->next = head;
		head->prev = n;
		return n;
	}

	Node *cur = head;
	while (cur->next != NULL && cur->next->data < data) {
		cur = cur->next;
	}
	n->prev = cur;
	n->next = cur->next;
	cur->next = n;

	return head;
}
