// HackerRank: Find Merge Point of Two Lists
// C++
// Author: Jimmy Mabey

int FindMergeNode(Node *headA, Node *headB) {
	Node *curA = headA, *curB;
	while (curA != NULL) {
		curB = headB;
		while (curB != NULL) {
			if (curA == curB) {
				return curA->data;
			}
			curB = curB->next;
		}
		curA = curA->next;
	}
	
	return 0;
}
