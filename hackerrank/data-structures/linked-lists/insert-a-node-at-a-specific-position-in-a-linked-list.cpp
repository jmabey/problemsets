// HackerRank: Insert a node at a specific position in a linked list
// C++
// Author: Jimmy Mabey

Node* InsertNth(Node *head, int data, int position) {
	Node *n = new Node;
	n->data = data;
	if (head == NULL) {
		return n;
	}
	if (position == 0) {
		n->next = head;
		return n;
	}

	Node *prev = head;
	for (int i = 1; i < position; i++) {
		prev = prev->next;
	}
	n->next = prev->next;
	prev->next = n;

	return head;
}
