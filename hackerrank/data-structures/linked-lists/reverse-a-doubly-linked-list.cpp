// HackerRank: Reverse a doubly linked list
// C++
// Author: Jimmy Mabey

Node* Reverse(Node* head) {
	Node *cur = head, *last = NULL;
	while (cur != NULL) {
		Node *tmp = cur->next;
		cur->next = cur->prev;
		cur->prev = tmp;

		last = cur;
		cur = tmp;
	}

	return last;
}
