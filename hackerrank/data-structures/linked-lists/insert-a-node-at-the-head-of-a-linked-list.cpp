// HackerRank: Insert a node at the head of a linked list
// C++
// Author: Jimmy Mabey

Node* Insert(Node *head, int data) {
	Node *n = new Node;
	n->data = data;
	n->next = head;

	return n;
}
