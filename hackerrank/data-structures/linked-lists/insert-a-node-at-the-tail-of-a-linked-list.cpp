// HackerRank: Insert a node at the tail of a linked list
// C++
// Author: Jimmy Mabey

Node* Insert(Node *head, int data) {
	Node *n = new Node;
	n->data = data;

	if (head == NULL) {
		return n;
	}

	Node *tail = head;
	while (tail->next != NULL) {
		tail = tail->next;
	}
	tail->next = n;

	return head;
}
