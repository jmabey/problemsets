// HackerRank: Merge two sorted linked lists
// C++
// Author: Jimmy Mabey

Node* MergeLists(Node *headA, Node* headB) {
	if (headA == NULL) {
		return headB;
	}
	if (headB == NULL) {
		return headA;
	}

	Node *a = headA, *b = headB, *next;
	while (b != NULL && b->data < a->data) {
		next = b->next;
		headA = b;
		b->next = a;
		b = next;
	}
	while (b != NULL) {
		while (a->next != NULL && b->data >= a->next->data) {
			a = a->next;
		}
		next = b->next;
		b->next = a->next;
		a = a->next = b;
		b = next;
	}

	return headA;
}
