// HackerRank: Print in Reverse
// C++
// Author: Jimmy Mabey

void ReversePrint(Node *head) {
	if (head != NULL) {
		ReversePrint(head->next);
		cout << head->data << endl;
	}
}
