// HackerRank: Delete a Node
// C++
// Author: Jimmy Mabey

Node* Delete(Node *head, int position) {
	if (position == 0) {
		Node *newHead = head->next;
		delete head;
		return newHead;
	}

	Node *prev = head, *next;
	for (int i = 1; i < position; i++) {
		prev = prev->next;
	}
	next = prev->next->next;
	delete prev->next;
	prev->next = next;

	return head;
}
