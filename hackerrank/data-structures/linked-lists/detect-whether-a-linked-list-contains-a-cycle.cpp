// HackerRank: Detect Cycle
// C++
// Author: Jimmy Mabey

int HasCycle(Node* head) {
	int len = 0;
	Node *cur = head;
	while (cur != NULL && len <= 100) {
		cur = cur->next;
		len++;
	}

	return len > 100;
}
