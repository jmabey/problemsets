// HackerRank: Print the elements of a linked list
// C++
// Author: Jimmy Mabey

void Print(Node *head) {
	while (head != NULL) {
		cout << head->data << endl;
		head = head->next;
	}
}
