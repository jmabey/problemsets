// HackerRank: Delete duplicate-value nodes from a sorted linked list
// C++
// Author: Jimmy Mabey

Node* RemoveDuplicates(Node *head) {
	Node *cur = head;
	while (cur != NULL) {
		if (cur->next != NULL && cur->next->data == cur->data) {
			cur->next = cur->next->next;
		} else {
			cur = cur->next;
		}
	}

	return head;
}
