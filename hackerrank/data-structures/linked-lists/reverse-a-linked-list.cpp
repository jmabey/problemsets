// HackerRank: Reverse a linked list
// C++
// Author: Jimmy Mabey

Node* Reverse(Node *head) {
	if (head == NULL || head->next == NULL) {
		return head;
	}

	Node *newHead = Reverse(head->next);
	head->next->next = head;
	head->next = NULL;
	return newHead;
}
