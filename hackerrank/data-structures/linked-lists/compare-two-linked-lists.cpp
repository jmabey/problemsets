// HackerRank: Compare two linked lists
// C++
// Author: Jimmy Mabey

int CompareLists(Node *headA, Node* headB) {
	Node *prevA = headA, *prevB = headB;
	while (prevA != NULL || prevB != NULL) {
		if ((prevA == NULL) != (prevB == NULL) || prevA->data != prevB->data) {
			return 0;
		}
		prevA = prevA->next;
		prevB = prevB->next;
	}

	return 1;
}
