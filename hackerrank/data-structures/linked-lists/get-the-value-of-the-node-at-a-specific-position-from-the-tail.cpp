// HackerRank: Get Node Value
// C++
// Author: Jimmy Mabey

int GetNode(Node *head, int positionFromTail) {
	int len = 0;
	Node *cur = head;
	while (cur != NULL) {
		cur = cur->next;
		len++;
	}

	int position = len - positionFromTail - 1;
	cur = head;
	while (position > 0) {
		cur = cur->next;
		position--;
	}

	return cur->data;
}
