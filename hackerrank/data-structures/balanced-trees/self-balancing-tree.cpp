// HackerRank: Self Balancing Tree
// C++
// Author: Jimmy Mabey

// update_height recalculates the height of node root. A height of 0 means root
// is a leaf node.
void update_height(node *root) {
	int lh = 0, rh = 0;
	if (root->left != NULL) {
		lh = root->left->ht + 1;
	}
	if (root->right != NULL) {
		rh = root->right->ht + 1;
	}
	root->ht = lh > rh ? lh : rh;
}

// balance_factor returns the balance factor of node root.
int balance_factor(node *root) {
	// Note a null subtree has height -1.
	int bf = 0;
	if (root->left != NULL) {
		bf = root->left->ht;
	} else {
		bf = -1;
	}
	if (root->right != NULL) {
		bf -= root->right->ht;
	} else {
		bf -= -1;
	}
	return bf;
}

// rotate_lr performs a left-right rotation beginning at node root.
void rotate_lr(node *root) {
	node *l = root->left, *r = root->left->right;
	root->left = r;
	l->right = r->left;
	r->left = l;

	update_height(l);
	update_height(r);
}

// rotate_ll performs a left-left rotation beginning at node root and returns
// the new root node.
node* rotate_ll(node *root) {
	node *l = root->left;
	root->left = l->right;
	l->right = root;

	update_height(root);
	update_height(l);

	return l;
}

// rotate_rl performs a right-left rotation beginning at node root.
void rotate_rl(node *root) {
	node *r = root->right, *l = root->right->left;
	root->right = l;
	r->left = l->right;
	l->right = r;

	update_height(r);
	update_height(l);
}

// rotate_rr performs a right-right rotation beginning at node root and returns
// the new root node.
node* rotate_rr(node *root) {
	node *r = root->right;
	root->right = r->left;
	r->left = root;

	update_height(root);
	update_height(r);

	return r;
}

// insert inserts val into the tree rooted by node root, maintains balance of
// the tree, and returns the new root node.
node* insert(node *root, int val) {
	if (root == NULL) {
		root = new node;
		root->val = val;
		root->ht = 1;
	} else if (val < root->val) {
		root->left = insert(root->left, val);
	} else {
		root->right = insert(root->right, val);
	}

	update_height(root);

	int bf = balance_factor(root);
	if (bf < -1) {
		if (balance_factor(root->right) > 0) {
			rotate_rl(root);
		}
		root = rotate_rr(root);
	} else if (bf > 1) {
		if (balance_factor(root->left) < 0) {
			rotate_lr(root);
		}
		root = rotate_ll(root);
	}

	return root;
}
