// HackerRank: Array and simple queries
// Go
// Author: Jimmy Mabey

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var n, m int
	fmt.Fscanln(in, &n, &m)

	a := make([]int, n+1)
	for i := 1; i <= n; i++ {
		fmt.Fscan(in, &a[i])
	}
	fmt.Fscanln(in)

	for i := 0; i < m; i++ {
		var t, j, k int
		fmt.Fscanln(in, &t, &j, &k)

		newA := []int{0}
		if t == 1 {
			newA = append(newA, a[j:k+1]...)
		}
		newA = append(newA, a[1:j]...)
		newA = append(newA, a[k+1:]...)
		if t == 2 {
			newA = append(newA, a[j:k+1]...)
		}
		a = newA
	}

	d := a[1] - a[n]
	if d < 0 {
		d *= -1
	}

	fmt.Println(d)
	fmt.Println(strings.Trim(fmt.Sprintf("%d", a[1:]), "[]"))
}
