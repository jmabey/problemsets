// HackerRank: Tree: Preorder Traversal
// C++
// Author: Jimmy Mabey

void Preorder(node *root) {
	cout << root->data << " ";
	if (root->left != NULL) {
		Preorder(root->left);
	}
	if (root->right != NULL) {
		Preorder(root->right);
	}
}
