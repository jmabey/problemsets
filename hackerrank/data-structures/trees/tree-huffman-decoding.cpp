// HackerRank: Tree: Huffman Decoding
// C++
// Author: Jimmy Mabey

void decode_huff(node *root, string s) {
	node *n = root;
	for (int i = 0; i < s.length(); i++) {
		if (s[i] == '0') {
			n = n->left;
		} else {
			n = n->right;
		}

		if (n->data != '\0') {
			cout << n->data;
			n = root;
		}
	}
}
