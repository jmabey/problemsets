// HackerRank: Tree: Inorder Traversal
// C++
// Author: Jimmy Mabey

void Inorder(node *root) {
	if (root->left != NULL) {
		Inorder(root->left);
	}
	cout << root->data << " ";
	if (root->right != NULL) {
		Inorder(root->right);
	}
}
