// HackerRank: Tree: Height of a binary tree
// C++
// Author: Jimmy Mabey

int height(node *root) {
	if (root == NULL) {
		return 0;
	}

	int l = height(root->left), r = height(root->right);
	return 1 + (l > r ? l : r);
}
