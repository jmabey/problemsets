// HackerRank: Binary Search Tree : Insertion
// C++
// Author: Jimmy Mabey

node* insert(node *root, int value) {
	if (root == NULL) {
		root = new node;
		root->data = value;
	} else if (value < root->data) {
		root->left = insert(root->left, value);
	} else {
		root->right = insert(root->right, value);
	}

	return root;
}
