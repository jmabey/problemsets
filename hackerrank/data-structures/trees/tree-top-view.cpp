// HackerRank: Tree : Top View
// C++
// Author: Jimmy Mabey

void top_view_recurse(node *root, bool left) {
	if (root == NULL) {
		return;
	}

	if (left) {
		top_view_recurse(root->left, true);
	}
	cout << root->data << " ";
	if (!left) {
		top_view_recurse(root->right, false);
	}
}

void top_view(node *root) {
	top_view_recurse(root->left, true);
	cout << root->data << " ";
	top_view_recurse(root->right, false);
}
