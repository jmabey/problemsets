// HackerRank: Swap Nodes [Algo]
// Go
// Author: Jimmy Mabey

package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	fmt.Scanln(&n)

	tree := Node{Value: 1}
	for i := 1; i <= n; i++ {
		var li, ri int
		fmt.Scanln(&li, &ri)

		tn := tree.Find(i)
		if li > 0 {
			tn.Left = &Node{Value: li}
		}
		if ri > 0 {
			tn.Right = &Node{Value: ri}
		}
	}

	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var d int
		fmt.Scanln(&d)

		tree.Swap(d)
		fmt.Println(strings.Trim(fmt.Sprintf("%d", tree.Inorder()), "[]"))
	}
}

// Node represents a node in a binary tree.
type Node struct {
	Value       int
	Left, Right *Node
}

// Find returns a pointer to a node with value v found in the subtree of n. If
// no node is round, nil is returned.
func (n *Node) Find(v int) *Node {
	if v == n.Value {
		return n
	}
	if n.Left != nil {
		if ln := n.Left.Find(v); ln != nil {
			return ln
		}
	}
	if n.Right != nil {
		if rn := n.Right.Find(v); rn != nil {
			return rn
		}
	}
	return nil
}

// Inorder returns a slice containing all node values ordered by inorder
// traversal.
func (n *Node) Inorder() []int {
	vs := []int{}
	if n.Left != nil {
		vs = append(vs, n.Left.Inorder()...)
	}
	vs = append(vs, n.Value)
	if n.Right != nil {
		vs = append(vs, n.Right.Inorder()...)
	}
	return vs
}

// Swap swaps all subtrees of nodes at the given depth and every multiple of
// depth.
func (n *Node) Swap(depth int) {
	var (
		d    = 1
		ns   = []*Node{n}
		next = []*Node{}
	)
	for len(ns) > 0 {
		var cur *Node
		cur, ns = ns[0], ns[1:]

		// Swap subtrees at every multiple of depth.
		if d%depth == 0 {
			cur.Left, cur.Right = cur.Right, cur.Left
		}

		// Queue nodes in the next depth to visit.
		if cur.Left != nil {
			next = append(next, cur.Left)
		}
		if cur.Right != nil {
			next = append(next, cur.Right)
		}

		// Visited all nodes at depth d, move on to the next depth.
		if len(ns) <= 0 {
			d++
			ns = next
			next = []*Node{}
		}
	}
}
