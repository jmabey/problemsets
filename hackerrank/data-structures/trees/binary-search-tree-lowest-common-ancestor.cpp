// HackerRank: Binary Search Tree : Lowest Common Ancestor
// C++
// Author: Jimmy Mabey

bool find(node *root, int v) {
	return root != NULL && (root->data == v || find(root->left, v) || find(root->right, v));
}

node* lca(node *root, int v1, int v2) {
	if (root == NULL) {
		return NULL;
	}

	// Recursively search left and right subtrees and return the result if both
	// are found.
	node *sub = lca(root->left, v1, v2);
	if (sub != NULL) {
		return sub;
	}
	sub = lca(root->right, v1, v2);
	if (sub != NULL) {
		return sub;
	}

	// Otherwise return this tree if both values are found.
	if (find(root, v1) && find(root, v2)) {
		return root;
	}

	return NULL;
}
