// HackerRank: Tree: Postorder Traversal
// C++
// Author: Jimmy Mabey

void Postorder(node *root) {
	if (root->left != NULL) {
		Postorder(root->left);
	}
	if (root->right != NULL) {
		Postorder(root->right);
	}
	cout << root->data << " ";
}
