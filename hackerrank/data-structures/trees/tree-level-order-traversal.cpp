// HackerRank: Tree: Level Order Traversal
// C++
// Author: Jimmy Mabey

#include <queue>

void LevelOrder(node *root) {
	queue<node*> q;
	q.push(root);

	while (!q.empty()) {
		node *n = q.front();
		q.pop();
		if (n == NULL) {
			continue;
		}

		cout << n->data << " ";
		q.push(n->left);
		q.push(n->right);
	}
}
