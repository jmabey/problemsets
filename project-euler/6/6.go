// Project Euler problem 6 solution (Sum square difference)
// Go 1.4.2
// Author: Jimmy Mabey

package main

import (
	"flag"
	"fmt"
	"math"
)

func main() {
	var max int
	flag.IntVar(&max, "m", 0, "use natural numbers 1 to this number")
	flag.Parse()

	var (
		sqsum = 0
		sumsq = 0
	)
	for n := 1; n <= max; n++ {
		sqsum += n
		sumsq += int(math.Pow(float64(n), 2))
	}
	sqsum = int(math.Pow(float64(sqsum), 2))

	fmt.Println(sqsum - sumsq)
}
