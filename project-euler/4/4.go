// Project Euler problem 4 solution (Largest palindrome product)
// Go 1.4.2
// Author: Jimmy Mabey

package main

import (
	"flag"
	"fmt"
	"math"
)

func main() {
	var d int
	flag.IntVar(&d, "d", 0, "number of digits")
	flag.Parse()

	var (
		max = int(math.Pow10(d)) - 1
		min = int(math.Pow10(d - 1))
		p   = 0
	)
	for i := max; i >= min; i-- {
		for j := max; j >= min; j-- {
			n := i * j
			if palindrome(n) && n > p {
				p = n
			}
		}
	}

	fmt.Println(p)
}

// palindrome returns whether or not n is a palindrome.
func palindrome(n int) bool {
	s := fmt.Sprint(n)
	for i := 0; i <= len(s)/2; i++ {
		if s[i] != s[len(s)-i-1] {
			return false
		}
	}
	return true
}
