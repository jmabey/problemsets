// Project Euler problem 1 solution (Multiples of 3 and 5)
// Go 1.3.1
// Author: Jimmy Mabey

package main

import (
	"flag"
	"fmt"
)

// sum returns the sum of all multiples of the given number under the given
// limit.
func sum(limit int, number int) int {
	if number <= 0 {
		return 0
	}

	last := (limit - 1) / number
	return number * last * (last + 1) / 2
}

func main() {
	var (
		lim int
		n   [2]int
	)

	flag.IntVar(&lim, "l", 0, "the upper limit of numbers to sum")
	flag.IntVar(&n[0], "n1", 0, "first number")
	flag.IntVar(&n[1], "n2", 0, "second number")
	flag.Parse()

	// Subtract common multiples so they are only counted once
	fmt.Println(sum(lim, n[0]) + sum(lim, n[1]) - sum(lim, n[0]*n[1]))
}
