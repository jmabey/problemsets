"""
Project Euler problem 1 solution (Multiples of 3 and 5)
Python 3.3.3
Author: Jimmy Mabey
"""

from argparse import ArgumentParser


def sum_multiples(limit, integer):
    """
    Return the sum of all multiples of the given integer under the given limit.
    """
    last = (limit - 1) // integer
    return integer * last * (last + 1) // 2


if __name__ == '__main__':
    parser = ArgumentParser(
        description='Calculate the sum of all multiples of two integers below '
                    'some limit.')
    parser.add_argument('limit', type=int)
    parser.add_argument('integers', metavar='integer', type=int, nargs=2)
    args = parser.parse_args()

    # Subtract common multiples so that they are only accounted for once
    print(
        sum_multiples(args.limit, args.integers[0]) +
        sum_multiples(args.limit, args.integers[1]) -
        sum_multiples(args.limit, args.integers[0] * args.integers[1]))
