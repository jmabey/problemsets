// Project Euler problem 5 solution (Smallest multiple)
// Go 1.4.2
// Author: Jimmy Mabey

package main

import (
	"flag"
	"fmt"
)

func main() {
	var max int
	flag.IntVar(&max, "m", 0, "find smallest multiple of 1 to this number")
	flag.Parse()

	n := max
	for !evenDivisible(n, max) {
		n += max
	}

	fmt.Println(n)
}

// evenDivisible returns whether or not n is evenly divisible by all numbers
// from 1 to max.
func evenDivisible(n, max int) bool {
	for i := 2; i <= max; i++ {
		if n%i != 0 {
			return false
		}
	}
	return true
}
