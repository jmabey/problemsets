// Project Euler problem 3 solution (Largest prime factor)
// Go 1.4.2
// Author: Jimmy Mabey

package main

import (
	"flag"
	"fmt"
	"math"
)

// largestPossibleFactor returns an int that may be the largest factor of n,
// which is sqrt(n).
func largestPossibleFactor(n int) int {
	return int(math.Sqrt(float64(n)))
}

// isFactor returns whether or not f is a factor of n.
func isFactor(f, n int) bool {
	return n%f == 0
}

// isPrime returns whether or not n is a prime number.
func isPrime(n int) bool {
	for f := largestPossibleFactor(n); f > 1; f-- {
		if isFactor(f, n) {
			return false
		}
	}
	return true
}

func main() {
	var n int

	flag.IntVar(&n, "n", 0, "number to search")
	flag.Parse()

	for f := largestPossibleFactor(n); f > 1; f-- {
		if isFactor(f, n) && isPrime(f) {
			fmt.Println(f)
			return
		}
	}

	fmt.Println("No largest prime factor found")
}
