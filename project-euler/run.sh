#!/bin/zsh
# Run solution for a given problem.

if [[ $# < 1 ]]
then
	echo "Usage: $0 number.ext args..."
	exit 1
fi

local number=${1:r}
local script="$number/$1"
local extension="${1:e}"

local args
args=($*[2,-1])

if [[ ! -f $script ]]
then
	echo "Solution file $script not found"
	exit 1
fi

if [[ $extension == "py" ]]
then
	python3 $script $args
elif [[ $extension == "go" ]]
then
	go run $script $args
else
	echo "Unrecognized extension $extension"
	exit 1
fi
