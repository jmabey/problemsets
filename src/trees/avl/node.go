package avl

import "fmt"

// Node represents a node in an AVL tree.
//
// A height of zero means the node is a leaf node. Size is a count of all nodes
// in the subtree rooted at this node, including itself.
type Node struct {
	Value        int
	Left, Right  *Node
	Height, Size int
}

// NewNode returns a new node initialized with value v.
func NewNode(v int) *Node {
	return &Node{Value: v, Size: 1}
}

// Balance returns the balance factor of node n.
func (n *Node) Balance() int {
	// A null subtree has height -1.
	lh := -1
	if n.Left != nil {
		lh = n.Left.Height
	}

	rh := -1
	if n.Right != nil {
		rh = n.Right.Height
	}

	return lh - rh
}

// bounds returns an error if index i is out of bounds of the tree rooted at
// node n.
func (n *Node) bounds(i int) error {
	if i < 0 || i >= n.Size {
		return fmt.Errorf("Index %d is out of bounds", i)
	}
	return nil
}

// rightIndex returns the lowest index of a node in the right subtree of node n,
// assuming a right subtree exists.
func (n *Node) rightIndex() int {
	if n.Left != nil {
		return 1 + n.Left.Size
	}
	return 1
}

// Index returns the node at index i in the tree rooted at node n.
func (n *Node) Index(i int) (*Node, error) {
	if err := n.bounds(i); err != nil {
		return nil, err
	}

	// Left subtree, if it exists, has indices 0 to n.Left.Size-1.
	if n.Left != nil && i < n.Left.Size {
		return n.Left.Index(i)
	}

	// Right subtree, if it exists, has indices 1+n.Left.Size to
	// n.Left.Size+n.Right.Size. Thus exploring the right subtree recursively
	// requires offsetting the index by 1+n.Left.Size.
	if ri := n.rightIndex(); n.Right != nil && i >= ri {
		return n.Right.Index(i - ri)
	}

	return n, nil
}

// Split splits the tree at the node with index i returning the root of the left
// and right resulting trees. Node i is the root of the left tree.
func (n *Node) Split(i int) (*Node, *Node, error) {
	if err := n.bounds(i); err != nil {
		return nil, nil, err
	}

	defer n.Update()

	// Node to split on is in left subtree:
	//
	//         n
	//        / \         l    n
	//       l   b  =>   /    / \
	//      / \         a    r   b
	//     a   r
	if n.Left != nil && i < n.Left.Size {
		l, r, err := n.Left.Split(i)
		if err != nil {
			return nil, nil, err
		}
		n.Left = r
		return l, n, nil
	}

	// Node to split on is in right subtree:
	//
	//       n
	//      / \           n    r
	//     a   r    =>   / \    \
	//        / \       a   l    b
	//       l   b
	if ri := n.rightIndex(); n.Right != nil && i >= ri {
		l, r, err := n.Right.Split(i - ri)
		if err != nil {
			return nil, nil, err
		}
		n.Right = l
		return n, r, nil
	}

	// Node to split on is this node:
	//
	//       n          n
	//      / \   =>   /   r
	//     l   r      l
	r := n.Right
	n.Right = nil
	return n, r, nil
}

// Join joins the trees with root nodes n and l into a single tree with root
// node n. l is joined to the leftmost node of the tree with root node n.
func (n *Node) Join(l *Node) {
	if n.Left != nil {
		n.Left.Join(l)
	} else {
		n.Left = l
	}

	n.Update()
}

// Update recalculates the height and size of node n.
func (n *Node) Update() {
	lh := 0
	if n.Left != nil {
		lh = n.Left.Height + 1
	}

	rh := 0
	if n.Right != nil {
		rh = n.Right.Height + 1
	}

	if lh > rh {
		n.Height = lh
	} else {
		n.Height = rh
	}

	n.Size = 1
	if n.Left != nil {
		n.Size += n.Left.Size
	}
	if n.Right != nil {
		n.Size += n.Right.Size
	}
}

// RotateLL performs a left-left rotation at root node n and returns the new
// root node.
//
//           X             Y
//          / \           / \
//         Y   d         /   \
//        / \     =>    Z     X
//       Z   c         / \   / \
//      / \           a   b c   d
//     a   b
func (n *Node) RotateLL() *Node {
	l := n.Left
	n.Left, l.Right = l.Right, n

	n.Update()
	l.Update()

	return l
}

// RotateLR performs a left-right rotation at root node n.
//
//         X              X
//        / \            / \
//       Y   d          Z   d
//      / \     =>     / \
//     a   Z          Y   c
//        / \        / \
//       b   c      a   b
func (n *Node) RotateLR() {
	l, r := n.Left, n.Left.Right
	n.Left, l.Right, r.Left = r, r.Left, l

	l.Update()
	r.Update()
}

// RotateRL performs a right-left rotation at root node n.
//
//       X            X
//      / \          / \
//     a   Y        a   Z
//        / \   =>     / \
//       Z   d        b   Y
//      / \              / \
//     b   c            c   d
func (n *Node) RotateRL() {
	r, l := n.Right, n.Right.Left
	n.Right, r.Left, l.Right = l, l.Right, r

	r.Update()
	l.Update()
}

// RotateRR performs a right-right rotation at root node n and returns the new
// root node.
//
//       X                 Y
//      / \               / \
//     a   Y             /   \
//        / \     =>    X     Z
//       b   Z         / \   / \
//          / \       a   b c   d
//         c   d
func (n *Node) RotateRR() *Node {
	r := n.Right
	n.Right, r.Left = r.Left, n

	n.Update()
	r.Update()

	return r
}

// Insert inserts a new node with value v into the tree rooted by node n. The
// balance of the tree is maintained and the new root node is returned.
func (n *Node) Insert(v int) *Node {
	if v < n.Value {
		if n.Left == nil {
			n.Left = NewNode(v)
		} else {
			n.Left = n.Left.Insert(v)
		}
	} else {
		if n.Right == nil {
			n.Right = NewNode(v)
		} else {
			n.Right = n.Right.Insert(v)
		}
	}

	n.Update()

	b := n.Balance()
	if b < -1 {
		if n.Right != nil && n.Right.Balance() > 0 {
			n.RotateRL()
		}
		n = n.RotateRR()
	} else if b > 1 {
		if n.Left != nil && n.Left.Balance() < 0 {
			n.RotateLR()
		}
		n = n.RotateLL()
	}

	return n
}
