package avl

import "testing"

var balanceTests = []struct {
	in  *Node
	out int
}{
	{&Node{}, 0},
	{&Node{Left: &Node{Height: 5}}, 6},
	{&Node{Right: &Node{Height: 5}}, -6},
	{&Node{Left: &Node{Height: 5}, Right: &Node{Height: 5}}, 0},
	{&Node{Left: &Node{Height: 6}, Right: &Node{Height: 5}}, 1},
	{&Node{Left: &Node{Height: 5}, Right: &Node{Height: 6}}, -1},
}

func TestBalance(t *testing.T) {
	for _, tt := range balanceTests {
		if b := tt.in.Balance(); b != tt.out {
			t.Errorf("Balance() => %d, want %d", b, tt.out)
		}
	}
}

func checkNode(t *testing.T, n *Node, v, h, s int) {
	if n.Value != v {
		t.Errorf("n.Value => %d, want %d", n.Value, v)
	}
	if n.Height != h {
		t.Errorf("n.Height => %d, want %d (value is %d)", n.Height, h, v)
	}
	if n.Size != s {
		t.Errorf("n.Size => %d, want %d (value is %d)", n.Size, s, v)
	}
}

func checkNilNode(t *testing.T, n *Node) {
	if n != nil {
		t.Errorf("got node %d, want nil", n.Value)
	}
}

func testTree() *Node {
	n := NewNode(1)
	for i := 2; i <= 7; i++ {
		n = n.Insert(i)
	}
	return n
}

var indexTests = []struct {
	in, v int
	err   string
}{
	{-1, 0, "Index -1 is out of bounds"},
	{0, 1, ""},
	{1, 2, ""},
	{2, 3, ""},
	{3, 4, ""},
	{4, 5, ""},
	{5, 6, ""},
	{6, 7, ""},
	{7, 0, "Index 7 is out of bounds"},
}

func TestIndex(t *testing.T) {
	n := testTree()
	for _, tt := range indexTests {
		found, err := n.Index(tt.in)
		goterr := ""
		if err != nil {
			goterr = err.Error()
		}
		if goterr != tt.err {
			t.Errorf("err => %q, want %q", goterr, tt.err)
		}
		if tt.err != "" {
			continue
		}
		if found.Value != tt.v {
			t.Errorf("found.Value => %d, want %d", found.Value, tt.v)
		}
	}
}

func TestSplit(t *testing.T) {
	n := testTree()
	_, _, err := n.Split(-1)
	if err == nil {
		t.Errorf("got no error, want out of bounds error")
	}

	l, r, err := n.Split(0)
	if err != nil {
		t.Errorf("Split(0): got error %q, want no error")
	}
	checkNode(t, l, 1, 0, 1)
	checkNode(t, r, 4, 2, 6)
	checkNilNode(t, r.Left.Left)

	n = testTree()
	l, r, err = n.Split(1)
	if err != nil {
		t.Errorf("Split(1): got error %q, want no error")
	}
	checkNode(t, l, 2, 1, 2)
	checkNode(t, l.Left, 1, 0, 1)
	checkNilNode(t, l.Right)
	checkNode(t, r, 4, 2, 5)
	checkNode(t, r.Left, 3, 0, 1)

	n = testTree()
	l, r, err = n.Split(2)
	if err != nil {
		t.Errorf("Split(2): got error %q, want no error")
	}
	checkNode(t, l, 2, 1, 3)
	checkNode(t, r, 4, 2, 4)
	checkNilNode(t, r.Left)

	n = testTree()
	l, r, err = n.Split(3)
	if err != nil {
		t.Errorf("Split(3): got error %q, want no error")
	}
	checkNode(t, l, 4, 2, 4)
	checkNilNode(t, l.Right)
	checkNode(t, r, 6, 1, 3)

	n = testTree()
	l, r, err = n.Split(4)
	if err != nil {
		t.Errorf("Split(4): got error %q, want no error")
	}
	checkNode(t, l, 4, 2, 5)
	checkNode(t, l.Right, 5, 0, 1)
	checkNode(t, r, 6, 1, 2)
	checkNilNode(t, r.Left)

	n = testTree()
	l, r, err = n.Split(5)
	if err != nil {
		t.Errorf("Split(5): got error %q, want no error")
	}
	checkNode(t, l, 4, 2, 6)
	checkNode(t, r, 7, 0, 1)
	checkNilNode(t, l.Right.Right)

	n = testTree()
	l, r, err = n.Split(6)
	if err != nil {
		t.Errorf("Split(6): got error %q, want no error")
	}
	checkNode(t, l, 4, 2, 7)
	checkNilNode(t, r)

	_, _, err = n.Split(7)
	if err == nil {
		t.Errorf("got no error, want out of bounds error")
	}
}

func TestJoin(t *testing.T) {
	l, r := testTree(), testTree()
	r.Join(l)
	checkNode(t, r, 4, 5, 14)
}

var updateTests = []struct {
	in           *Node
	height, size int
}{
	{&Node{}, 0, 1},
	{&Node{Left: &Node{Height: 5, Size: 4}}, 6, 5},
	{&Node{Right: &Node{Height: 5, Size: 4}}, 6, 5},
	{&Node{Left: &Node{Height: 3, Size: 2}, Right: &Node{Height: 6, Size: 5}}, 7, 8},
}

func TestUpdate(t *testing.T) {
	for _, tt := range updateTests {
		tt.in.Update()
		checkNode(t, tt.in, 0, tt.height, tt.size)
	}
}

func TestRotateLL(t *testing.T) {
	n := &Node{Value: 10, Height: 3, Size: 7}
	n.Left = &Node{Value: 20, Height: 2, Size: 5}
	n.Left.Left = &Node{Value: 30, Height: 1, Size: 3}

	n.Left.Left.Left = NewNode(1)
	n.Left.Left.Right = NewNode(2)
	n.Left.Right = NewNode(3)
	n.Right = NewNode(4)

	n = n.RotateLL()

	checkNode(t, n, 20, 2, 7)
	checkNode(t, n.Left, 30, 1, 3)
	checkNode(t, n.Left.Left, 1, 0, 1)
	checkNode(t, n.Left.Right, 2, 0, 1)
	checkNode(t, n.Right, 10, 1, 3)
	checkNode(t, n.Right.Left, 3, 0, 1)
	checkNode(t, n.Right.Right, 4, 0, 1)
}

func TestRotateLR(t *testing.T) {
	n := &Node{Value: 10, Height: 3, Size: 7}
	n.Left = &Node{Value: 20, Height: 2, Size: 5}
	n.Left.Right = &Node{Value: 30, Height: 1, Size: 3}

	n.Left.Left = NewNode(1)
	n.Left.Right.Left = NewNode(2)
	n.Left.Right.Right = NewNode(3)
	n.Right = NewNode(4)

	n.RotateLR()

	checkNode(t, n, 10, 3, 7)
	checkNode(t, n.Left, 30, 2, 5)
	checkNode(t, n.Left.Left, 20, 1, 3)
	checkNode(t, n.Left.Left.Left, 1, 0, 1)
	checkNode(t, n.Left.Left.Right, 2, 0, 1)
	checkNode(t, n.Left.Right, 3, 0, 1)
	checkNode(t, n.Right, 4, 0, 1)
}

func TestRotateRL(t *testing.T) {
	n := &Node{Value: 10, Height: 3, Size: 7}
	n.Right = &Node{Value: 20, Height: 2, Size: 5}
	n.Right.Left = &Node{Value: 30, Height: 1, Size: 3}

	n.Left = NewNode(1)
	n.Right.Left.Left = NewNode(2)
	n.Right.Left.Right = NewNode(3)
	n.Right.Right = NewNode(4)

	n.RotateRL()

	checkNode(t, n, 10, 3, 7)
	checkNode(t, n.Left, 1, 0, 1)
	checkNode(t, n.Right, 30, 2, 5)
	checkNode(t, n.Right.Left, 2, 0, 1)
	checkNode(t, n.Right.Right, 20, 1, 3)
	checkNode(t, n.Right.Right.Left, 3, 0, 1)
	checkNode(t, n.Right.Right.Right, 4, 0, 1)
}

func TestRotateRR(t *testing.T) {
	n := &Node{Value: 10, Height: 3, Size: 7}
	n.Right = &Node{Value: 20, Height: 2, Size: 5}
	n.Right.Right = &Node{Value: 30, Height: 1, Size: 3}

	n.Left = NewNode(1)
	n.Right.Left = NewNode(2)
	n.Right.Right.Left = NewNode(3)
	n.Right.Right.Right = NewNode(4)

	n = n.RotateRR()

	checkNode(t, n, 20, 2, 7)
	checkNode(t, n.Left, 10, 1, 3)
	checkNode(t, n.Left.Left, 1, 0, 1)
	checkNode(t, n.Left.Right, 2, 0, 1)
	checkNode(t, n.Right, 30, 1, 3)
	checkNode(t, n.Right.Left, 3, 0, 1)
	checkNode(t, n.Right.Right, 4, 0, 1)
}

func TestInsert(t *testing.T) {
	n := NewNode(10)

	n = n.Insert(5)
	checkNode(t, n, 10, 1, 2)
	checkNode(t, n.Left, 5, 0, 1)

	n = n.Insert(15)
	checkNode(t, n, 10, 1, 3)
	checkNode(t, n.Left, 5, 0, 1)
	checkNode(t, n.Right, 15, 0, 1)

	n = NewNode(10)
	n = n.Insert(5)
	n = n.Insert(2)
	checkNode(t, n, 5, 1, 3)
	checkNode(t, n.Left, 2, 0, 1)
	checkNode(t, n.Right, 10, 0, 1)

	n = NewNode(10)
	n = n.Insert(5)
	n = n.Insert(7)
	checkNode(t, n, 7, 1, 3)
	checkNode(t, n.Left, 5, 0, 1)
	checkNode(t, n.Right, 10, 0, 1)

	n = NewNode(10)
	n = n.Insert(15)
	n = n.Insert(12)
	checkNode(t, n, 12, 1, 3)
	checkNode(t, n.Left, 10, 0, 1)
	checkNode(t, n.Right, 15, 0, 1)

	n = NewNode(10)
	n = n.Insert(15)
	n = n.Insert(20)
	checkNode(t, n, 15, 1, 3)
	checkNode(t, n.Left, 10, 0, 1)
	checkNode(t, n.Right, 20, 0, 1)
}
