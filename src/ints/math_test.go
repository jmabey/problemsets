package ints

import "testing"

var gcdTests = []struct {
	in  [2]int
	out int
}{
	{[2]int{2, 3}, 1},
	{[2]int{100, 50}, 50},
	{[2]int{-100, -50}, 50},
	{[2]int{5, 0}, 5},
	{[2]int{0, 5}, 5},
	{[2]int{0, 0}, 0},
	{[2]int{7, 21}, 7},
}

func TestGCD(t *testing.T) {
	for _, tt := range gcdTests {
		if v := GCD(tt.in[0], tt.in[1]); v != tt.out {
			t.Errorf("GCD(%d, %d) => %d, want %d", tt.in[0], tt.in[1], v, tt.out)
		}
	}
}
