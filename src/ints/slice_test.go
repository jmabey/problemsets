package ints

import (
	"reflect"
	"testing"
)

var intToSliceTests = []struct {
	in  int
	out []int
}{
	{0, []int{0}},
	{1, []int{1}},
	{12345, []int{5, 4, 3, 2, 1}},
	{-12345, []int{-5, -4, -3, -2, -1}},
}

func TestIntToSlice(t *testing.T) {
	for _, tt := range intToSliceTests {
		if s := IntToSlice(tt.in); !reflect.DeepEqual(s, tt.out) {
			t.Errorf("IntToSlice(%d) => %d, want %d", tt.in, s, tt.out)
		}
	}
}

var sliceToIntTests = []struct {
	in  []int
	out int
}{
	{[]int{}, 0},
	{[]int{0}, 0},
	{[]int{1}, 1},
	{[]int{5, 4, 3, 2, 1}, 12345},
	{[]int{-5, -4, -3, -2, -1}, -12345},
}

func TestSliceToInt(t *testing.T) {
	for _, tt := range sliceToIntTests {
		if v := SliceToInt(tt.in); v != tt.out {
			t.Errorf("SliceToInt(%d) => %d, want %d", tt.in, v, tt.out)
		}
	}
}
