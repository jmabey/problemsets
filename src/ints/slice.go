package ints

// IntToSlice converts n into a slice (lowest place value first) containing its
// digits. If n is negative, all digits in the slice are negative.
func IntToSlice(n int) []int {
	var ds []int
	if n == 0 {
		ds = append(ds, 0)
	}
	for n != 0 {
		ds = append(ds, n%10)
		n /= 10
	}
	return ds
}

// SliceToInt converts s, representing the digits of a number (lowest place
// value first), into an int.
func SliceToInt(s []int) int {
	n := 0
	for i := len(s) - 1; i >= 0; i-- {
		n = n*10 + s[i]
	}
	return n
}
